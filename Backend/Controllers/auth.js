var passport = require('passport');
var account = require('../handlers/tables/h_account.js');

var sendJSONresponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.register = function(req, res) {

  //by default only normal user can be registered on the frontend
  account.create(req.body.userName, req.body.pwd, function(e, data){
    if(e){
      res.status(500).end();
    }else{
      //return the jw token
      res.writeHead(200, {'Content-type': 'application/json'});
      res.write(JSON.stringify({token: data}));
      res.end();
    }
  })
};

module.exports.login = function(req, res, next) {
  passport.authenticate('local', function(err, user, info){
    var token;
    console.log("passport authentication");
    // If Passport throws/catches an error
    if (err) {
      res.status(404).json(err);
      return;
    }

    // If a user is found
    if(user){
      console.log("Creating jw token...");
      console.log("out user: ")
      console.log(user);
      token = account.generateJwt(user);
      res.status(200);
      res.json({
        "token" : token
      });
    } else {
      // If user is not found
      res.status(401).json(info);
    }
  })(req, res, next);

};