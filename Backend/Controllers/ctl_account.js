/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

/*controller to handle everything about Account*/

var logger = require('../../Config/logger.js');

//loading the db table handler
var account = require('../handlers/tables/h_account.js');
var users = require('../handlers/tables/h_user.js');


module.exports = {
	create:function(req,res){
		logger.info("ctl_account:create");
		//req.body.idRoles should not be given - in this case we're assuming allways 3 (normal user)
		//call db hander
		account.create(req.body.userName, req.body.pwd, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));	
			}
			res.end();
		});
	},
	delete:function(req,res){
		logger.info("ctl_account:delete");
		//call db handler
		account.delete(req.params.accountID, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	updateName: function(req,res){
		logger.info("ctl_account:updateName");
		//call db handler
		account.updateNamebyID(req.body.id, req.body.username, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	updateRole: function(req,res){
		logger.info("ctl_account:updateRole");
		//call db handler
		account.updateRolebyID(req.body.id, req.body.role, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	updatePassword: function(req,res){
		logger.info("ctl_account:updatePassword");
		//call db handler
		account.updatePassword(req.body.id, req.body.pwd, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	getAllAccounts: function(req,res){
		logger.info("ctl_account:getAllAccounts");
		//call db handler
		if(req.query.idaccounts){
			account.getAllAccounts(req.query.idaccounts, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, data: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, data: result}));		
				}
				res.end();
			})
		}else{
			res.writeHead(500, {'Content-type': 'application/json'});
			res.write(JSON.stringify({Result: false, Reason: "Wrong Query"}));
		}
	},
	/*
	getAllAccounts: function(req,res){
		logger.info("ctl_account:getAllAccounts");
		//call db handler
		if(req.query.idaccounts){
			users.getByAccountId(req.query.idaccounts, function(e, res){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, data: e}));
				}else{
					account.getAllAccounts(res, function(e, result){
						if(e){
							logger.error(e);
							res.writeHead(500, {'Content-type': 'application/json'});
							res.write(JSON.stringify({Result: false, data: e}));
						}else{
							res.writeHead(200, {'Content-type': 'application/json'});
							res.write(JSON.stringify({Result: true, data: result}));		
						}
						res.end();
					})
				}
			})
		}else{
			res.writeHead(500, {'Content-type': 'application/json'});
			res.write(JSON.stringify({Result: false, Reason: "Wrong Query"}));
		}
	},
	*/
	getbyID: function(req,res){
		logger.info("ctl_account:getbyID");
		//call db handler
		account.getbyID(req.params.accountID, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, data: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	getbyUserName: function(req,res){
		logger.info("ctl_account:getbyUserName");
		//call db handler
		account.getbyUserName(req.params.userName, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, data: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	}
}