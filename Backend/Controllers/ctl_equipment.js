/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

/*controller to handle everything about Equipment*/

var logger = require('../../Config/logger.js');

//loading the db table handler
var equipment = require('../handlers/tables/h_equipment.js');


module.exports = {
	create:function(req,res){
		logger.info("ctl_account:create");
		//call db hander
		equipment.create(req.body.name, req.body.idProjects, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));
			}
			res.end();
		});
	},
	delete:function(req,res){
		logger.info("ctl_account:delete");
		//call db handler
		if(req.query.idequipment){
			equipment.delete(req.query.idequipment, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, Reason: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, Reason: result}));		
				}
				res.end();
			})
		}else{
			res.writeHead(500, {'Content-type': 'application/json'});
			res.write(JSON.stringify({Result: false, Reason: "Wrong Query"}));
		}
	},
	get: function(req,res){
		logger.info("ctl_account:get");
		//call db handler
		if(req.query.idproject){
			equipment.getbyProjectID(req.query.idproject, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, data: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, Reason: result}));		
				}
				res.end();
			})
		}else{
			res.writeHead(500, {'Content-type': 'application/json'});
			res.write(JSON.stringify({Result: false, Reason: "Wrong Query"}));
		}
	}
}