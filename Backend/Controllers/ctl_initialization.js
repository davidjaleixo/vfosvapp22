/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

/*controller to handle everything about Initialization*/

var logger = require('../../Config/logger.js');

//loading the db table handler
var dbinit = require('../handlers/tables/h_initialization.js');


module.exports = {
	init:function(){
		logger.info("ctl_initialization:init");
		console.log("ctl_initialization:init");

		//call db hander
		dbinit.init(function(e, message){
			if(e){
				//res.status(500).json({Result: true, Reason: message});
				//res.end();
				console.log(message);
				console.log(e);
			}else{
				//res.status(200).json({Result: true, Reason: message})
				//res.end();
			console.log("db init success");
			console.log(message);
			}
		})
	}
}