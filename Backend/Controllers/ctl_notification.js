var logger = require('../../Config/logger.js');

//loading the db table handler
var project = require('../handlers/tables/h_project.js');
var slumptest = require('../handlers/tables/h_slumptest.js');
var user = require('../handlers/tables/h_user.js')
var notification = require('../handlers/tables/h_notification.js')

var configuration = require('../../config.json');
var request = require('request');
var auth	= require('./auth');

module.exports={
	validate:function(req,res, next){
		logger.info("ctl_notification:validate");
		
		//CALL NOTIFICATION ENABLER **********
		var accList=[];
		//check if the threshold value is less/bigger than the slumpttest value
		project.getbyID(req.body.project, function(e,project){
			if(e){
				res.status(500).json(e);
			}else{
				if (req.body.value >= project.threshold){
					logger.debug("ctl_notification:validade we have a NOK slumptest")
					//get all the accounts related to this project
					user.getByProjectId(req.body.project, function(e,userList){
						if(e){
							res.status(500).json(e)
						}else{
							logger.debug("this is our user list: ");
							logger.debug(userList);
							//grab the list of accountsid
							userList.forEach(function(eachUser, idx, array){
								accList.push(eachUser.idaccounts);
								if(idx == array.length -1 ){
									logger.debug("we are done!! with: ");
									logger.debug(accList);
									slumptest.getLastbyAccountID(req.body.account,function(err, lastSlumpTest){
										if(err){
											res.status(500).json(err);
										}else{
											//Save Notification to DB
											notification.create(accList, lastSlumpTest.idslumptests, lastSlumpTest.date, function(err2, result){
												if(err2){
													res.status(500).json(err2);
												}else{
												//Call Enabller_Framework.Notification_Enabler
													CallNotificationEnabler(req.body.value).then(
														function(data){

															if (data>0){
																res.status(200).json(result);
																//res.status(200).json("Sent Notification: Successfully");
															}
															else{
																res.status(200).json("Notification Created but Alert not sent");
															}
														}
														,function(rej){
															res.status(500).json("Notification Enabler Error");
														}
													);
												}
											})
										}
									})	
								}
							})
						}
					})
				}else{
					res.status(200).json("SlumpTest is Lower than Threshold");
				}
			}
		})
	},
	update: function(req,res){
		logger.info("ctl_notification:update");
		//call db handler
		notification.update(req.body, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	get: function(req,res){
		logger.info("ctl_notification:get");
		if(req.query.accid){
			notification.getByAccId(req.query.accid, function(e,r){
				if(e){
					res.status(500).json(e)
				}else{
					res.status(200).json(r)
				}
			})
		}else{
			res.status(400);
		}
	}
}

var CallNotificationEnabler = async function(slumptestresult){

	// prepare request to the notification enabler 
	var Rbody= JSON.stringify(	{
		"token":configuration.EnablerFramework.NotificationEnabler.AppToken,
		"body":[
			{
			"subject":configuration.EnablerFramework.NotificationEnabler.Parameter,
			 "subjectValue": slumptestresult
			}
		]
	});
	var Rheaders = {
		'Content-Type':     'application/json'
	}
	
	var options = {
		url: configuration.EnablerFramework.NotificationEnabler.url_api+'notifications',
		method: 'POST',
		headers: Rheaders,
		body: Rbody
		//form: {'key1': 'xxx', 'key2': 'yyy'}
	}	
	
	// send request to the notification enabler 
	return new Promise(function (resolve, reject){
	
		request(options, function (error, response) {				
			if (!error ) {						
				if (response.statusCode == 200){
					// Print out the response body
					parsedBody = JSON.parse(response.body);	
					alertStatus = parsedBody.reason[0].results[0].success;		
					if (alertStatus==true){
						logger.debug('Notification Enabler ctrl: Alert Sent');
						//return(2);
						resolve(2);
					}					
					else{
						//console.log(error,response.body);
						logger.debug('Notification Enabler ctrl : Test Result is OK, no need to send Notification')
						resolve(1);
					}
				}
				else{
					//console.log("Notification Enabller Error");
					//console.log(error,response.body);
					logger.debug('Notification Enabler ctrl: Notification Service Problem')
					reject(-1);
				}					
			}		
			else{
				//console.log("Notification Enabller not responding");
				logger.debug('Notification Enabler ctrl: Notification Enabller not responding')
				reject(-2);
			}				
		})	
	})
	//sneding response back to the front end
}