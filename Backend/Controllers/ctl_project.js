/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

/*controller to handle everything about Project*/

var request = require('request');
var configuration = require('../../config.json');

var logger = require('../../Config/logger.js');

//loading the db table handler
var project = require('../handlers/tables/h_project.js');
var user = require('../handlers/tables/h_user');
var slumptest = require('../handlers/tables/h_slumptest');

var getByAccountId = function(accid, cb){
	logger.info("getByAccount");
	var projectList = [];
	user.getByAccountId(accid,function(e,relation){
		if(e){
			cb(true,e)
		}
		logger.debug("relationship between accounts and projects: " + JSON.stringify(relation))
		relation.forEach(function(eachItem, idx, array){
			project.getbyID(eachItem.idprojects, function(e,eachProject){
				if(e){
					logger.error(e)
				}else{
					if(eachProject != null){
						projectList.push(eachProject);
					}
				}
				logger.debug(eachItem);
				logger.debug(idx);
				logger.debug(array.length - 1);
				//Stop Checking
				if(idx === array.length - 1 ){
					logger.debug("ctl_projects:found " + (idx+1) + " projects related to account " + accid);

					setTimeout(function(){
						cb(false, projectList)
					}, 1000);
				}
			})
		})
	})
}

module.exports = {
	create:function(req,res){
		logger.info("ctl_project:create");
		//call db hander
		project.create(req.body.name, req.body.description, function(e, result){
			if(e){
				logger.error(e);
				res.status(500).json(e);
			}else{
				project.getbyName(req.body.name, function(err1,r){
					if(err1){
						logger.error(err1);
						res.status(500).json(err1);
					}else{
						logger.debug("new project was created with id: " + r.idprojects);
						//update the user's table
						user.create(req.query.accid, r.idprojects, function(err2,response){
							if(err2){
								logger.error(err2);
								res.status(500).json(err2);
							}else{
								res.status(200).json({Result: true, Reason: result})
							}
						})
					}
					
				})	
			}
		});
	},
	delete:function(req,res){
		logger.info("ctl_project:delete");
		//call db handler
		if(req.query.idproject){
			project.deletebyID(req.query.idproject, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, Reason: e}));
				}else{
					user.deletebyProjectID(req.query.idproject, function(e, result){
						if(e){
							logger.error(e);
							res.writeHead(500, {'Content-type': 'application/json'});
							res.write(JSON.stringify({Result: false, Reason: e}));
						}else{
							slumptest.delete(req.query.idproject, function(e, response){
								if(e){
									logger.error(e);
									res.writeHead(500, {'Content-type': 'application/json'});
									res.write(JSON.stringify({Result: false, Reason: e}));
								}else{
									res.writeHead(200, {'Content-type': 'application/json'});
									res.write(JSON.stringify({Result: true, Reason: response}));	
								}
								res.end();
							})	
						}
					})	
				}			
			})
		}
	},
	updateName: function(req,res){
		logger.info("ctl_project:updateName");
		//call db handler
		project.updateNamebyID(req.body.id, req.body.name, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	updateDescription: function(req,res){
		logger.info("ctl_project:updateDescription");
		//call db handler
		project.updateDescriptionbyID(req.body.id, req.body.description, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	updateStatus: function(req,res){
		logger.info("ctl_project:updateStatus");
		//call db handler
		project.updateStatusbyID(req.body.id, req.body.status, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	updateThold: function(req,res){
		logger.info("ctl_project:updateThold");
		var NEWthreshold=req.body.thold;
		//call db handler
		project.updateTholdbyID(req.body.id, req.body.thold, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
		//cal Notification Enabler for updating the Control Value
		CallNotificationEnablerUpdateThreshold(NEWthreshold)
	},
	get:function(req, res){
		logger.info("ctl_project:get");
		if(req.query.accid){
			getByAccountId(req.query.accid, function(e,r){
				if(e){
					res.status(500).json(e)
				}else{
					logger.debug(r);
					res.status(200).json({Result: true, Reason: r})
				}
			})
		}
	},
	getbyID: function(req,res){
		logger.info("ctl_project:getbyID");
		//call db handler
		if(req.query.idproject){
			project.getbyID(req.query.idproject, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, data: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, Reason: result}));		
				}
				res.end();
			})
		}else{
			res.writeHead(500, {'Content-type': 'application/json'});
			res.write(JSON.stringify({Result: false, Reason: "Wrong Query"}));
		}
	}
}

var CallNotificationEnablerUpdateThreshold = function(Threshold){
	// send request to the notification enabler 
	var data = {
		body:[{
			rulesid: "5",//1
			description: "test going bad",
			parameter: configuration.EnablerFramework.NotificationEnabler.Parameter,
			conditionValue: ">=",
			controlValue: Threshold,
			threshold: "100",
			notifyType: "Email",
			notificationType: "5",
			hostname: "",
			port: "",
			path: "",
			method: "",
			emailTo:[
				configuration.EnablerFramework.NotificationEnabler.notificationID
			]
		}]
	};
	request(
		{ 
			url: configuration.EnablerFramework.NotificationEnabler.url_api+'edit', 
			method: 'PUT', 
			headers: {
				'Content-Type':     'application/json'
			},
			body: JSON.stringify(data)
		}, 
		function (err, data){
			if (err){
				logger.error(" Notification Enabler Editrule not accessible"+err);
			}
			else{ 
			   logger.debug(JSON.stringify(data));
			}
		}
		    )
}