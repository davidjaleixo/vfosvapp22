/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

/*controller to handle everything about Role*/

var logger = require('../../Config/logger.js');

//loading the db table handler
var role = require('../handlers/tables/h_role.js');


module.exports = {
	create:function(req,res){
		logger.info("ctl_role:create");
		//call db hander
		role.create(req.body.accountType, req.body.description, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		});
	},
	delete:function(req,res){
		logger.info("ctl_role:delete");
		//call db handler
		if(req.query.idrole){
			role.delete(req.query.idrole, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, Reason: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, Reason: result}));		
				}
				res.end();
			})
		}else{
			res.writeHead(500, {'Content-type': 'application/json'});
			res.write(JSON.stringify({Result: false, Reason: "Wrong Query"}));
		}
	},
	get: function(req,res){
		logger.info("ctl_role:get");
		//call db handler
		role.get(function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, data: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	}
}