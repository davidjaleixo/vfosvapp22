/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

/*controller to handle everything about Slumptest*/

var logger = require('../../Config/logger.js');

//loading the db table handler
var slumptest = require('../handlers/tables/h_slumptest.js');


module.exports = {
	create:function(req,res,next){
		logger.info("ctl_slumptest:create");
		//call db hander
		slumptest.create(req.body.value, req.body.equipment, req.body.project, req.body.account, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
				res.end();
			}else{
				next();
			}
		});
	},
	get: function(req,res){
		logger.info("ctl_slumptest:get");
		//call db handler
		if(req.query.idslumptest){
			slumptest.getbyID(req.query.idslumptest, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, data: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, data: result}));
				}
				res.end();
			})
		}else if(req.query.idproject){
			slumptest.getbyProjectIDHistory(req.query.idproject, function(e, result){
				if(e){
					logger.error(e);
					res.writeHead(500, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: false, data: e}));
				}else{
					res.writeHead(200, {'Content-type': 'application/json'});
					res.write(JSON.stringify({Result: true, data: result}));
				}
				res.end();
			})
		}
	}
}