/*made KBZ */

var logger = require('../../Config/logger.js');

//loading the db table handler
var bars = require('../handlers/tables/h_bars.js');


module.exports = {
	create: function (req, res) {
		logger.info("ctl_stelbar:create");
		console.log("body : ", req.body);

		if (req.body.type && req.body.idprojects) {
			//call db hander
			bars.create(req.body.type, req.body.idprojects, req.body.photo, req.body.photoTransport, req.body.photoMarkings, function (e, result) {
				if (e) {
					logger.error(e);
					res.writeHead(500, { 'Content-type': 'application/json' });
					res.write(JSON.stringify({ Result: false, Reason: e }));
				} else {
					res.writeHead(200, { 'Content-type': 'application/json' });
					res.write(JSON.stringify({ Result: true, Reason: result }));
				}
				res.end();
			});
		}else{
			res.status(422).json({ message: "Missing required field" })
		}

	},
	get: function (req, res) {
		logger.info("ctl_users:get");
		bars.get(req.params.projectid, function (e, result) {
			if (e) {
				logger.error(e);
				res.writeHead(500, { 'Content-type': 'application/json' });
				res.write(JSON.stringify({ Result: false, data: e }));
			} else {
				res.writeHead(200, { 'Content-type': 'application/json' });
				res.write(JSON.stringify({ Result: true, Reason: result }));
			}
			res.end();
		})
	},
	delete: function (req, res) {
		//call db handler
		bars.delete(req.params.tagType, req.params.projectID, function (e, result) {
			if (e) {
				logger.error(e);
				res.writeHead(500, { 'Content-type': 'application/json' });
				res.write(JSON.stringify({ Result: false, Reason: e }));
			} else {
				res.writeHead(200, { 'Content-type': 'application/json' });
				res.write(JSON.stringify({ Result: true, Reason: result }));
			}
			res.end();
		})
	},
}