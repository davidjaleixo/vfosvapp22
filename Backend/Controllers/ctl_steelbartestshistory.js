/*made KBZ */

var logger = require('../../Config/logger.js');

//loading the db table handler
var barstest = require('../handlers/tables/h_barstest.js');


module.exports = {
	create:function(req,res){
		logger.info("ctl_steelbarTest:create");
        //call db hander
		barstest.create(req.body.bartestprojectid, req.body.bartestapproved,req.body.bartestdetected,req.body.bartestok,req.body.bartestdatetime,req.body.photoTransport, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		});
	},
	get:function(req,res){
		console.log("ctl_steelbarTest:get");
		logger.info("ctl_steelbarTest:get");
		barstest.get(req.params.projectid, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, data: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})		
	},
}