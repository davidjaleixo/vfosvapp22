/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

var logger = require('../../Config/logger.js');

//loading the db table handler
var users = require('../handlers/tables/h_user.js');


module.exports = {
	create:function(req,res){
		logger.info("ctl_users:create");
		//req.body.idRoles should not be given - in this case we're assuming allways 3 (normal user)
		//call db hander
		users.create(req.body.accountid, req.body.projectid, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		});
	},
	delete: function(req,res){
		logger.info("we are on the users controller - handler delete");
		//call db handler
		users.deletebyID(req.params.accountID, req.params.projectID, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, Reason: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	},
	get:function(req,res){
		logger.info("ctl_users:get");
		users.getByProjectId(req.params.projectid, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, data: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})		
	},
	getUsersNotOnProject:function(req,res){
		logger.info("ctl_users:get Special users ");
		users.getUsersNotOnProject(req.params.projectid, function(e, result){
			if(e){
				logger.error(e);
				res.writeHead(500, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: false, data: e}));
			}else{
				res.writeHead(200, {'Content-type': 'application/json'});
				res.write(JSON.stringify({Result: true, Reason: result}));		
			}
			res.end();
		})
	}
}