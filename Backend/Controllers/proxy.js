// PROXY
var request = require('request');
var path = require('path');
var configuration = require('../../config.json');
var fs = require('fs');
var logger = require('../../Config/logger.js');

var FormData = require('form-data');

var multiparty = require('multiparty');

module.exports = {

    proxyIt: function (req, res) {
        logger.info("ctl_proxy");
        // request.post(configuration.mlUrl).pipe(res);
        var formdata = new FormData();
        // var incomingForm = new multiparty.Form();
        console.log("trying to read ", path.join(path.normalize(__dirname), '../../uploads'))
        fs.readdir(path.join(path.normalize(__dirname), '../../uploads'), function (err, list) {
            console.log("FILES IN FOLDER:",list);
            if (list.length == 2) {
                console.log("Just one image file found... using it")
                console.log("FILE TO USE: ", path.join(path.normalize(__dirname), '../../uploads/' + list[1]));
                formdata.append('file', fs.createReadStream(path.join(path.normalize(__dirname), '../../uploads/' + list[1])));
                console.log("formdata", formdata.getHeaders());
                // formdata.append('file','asdf',)
                // xhr.onreadystatechange = function () {
                //     console.log("Proxy State: " + this.readyState);

                //     if (this.readyState === 4) {
                //         console.log("Complete.\nBody length: " + this.responseText.length);
                //         console.log("Body:\n" + this.responseText);
                //         console.log("Returning response from ML...");
                //         console.log("Deleting file...");
                //         fs.unlink(path.join(path.normalize(__dirname), '../../uploads/', list[0]), (err) => {
                //             if (err) throw err;
                //             console.log(path.join(path.normalize(__dirname), '../../uploads/', list[0]), ' was deleted');
                //         });
                //         res.status(200).send(this.responseText);
                //     }
                // };
                // console.log("POSTING TO ", configuration.mlUrl);

                // xhr.open("POST", configuration.mlUrl, true);
                // xhr.send(formdata);
                fs.unlink(path.join(path.normalize(__dirname), '../../uploads/', list[1]), (err) => {
                    if (err) throw err;
                    console.log(path.join(path.normalize(__dirname), '../../uploads/', list[1]), ' was deleted');
                });
                console.log("proxing...");
                var Requestoptions = {
                    url: configuration.mlUrl,
                    method: 'POST',
                    headers: formdata.getHeaders(),
                    body: formdata
                }
                request(Requestoptions, function (error, proxyresponse, proxybody) {
                    if (!error) {
                        console.log("ML has answered with:", proxybody);
                        res.status(200).send(proxybody);
                    } else {
                        console.log("ML error:", error);
                        res.status(500).send(error);
                    }

                })
            }else{
                res.status(500).send("More than one file in dir");
            }
            // list.forEach(function (file) {
            //     console.log(path.join(path.normalize(__dirname), '../../uploads/', file));
            //     stats = fs.statSync(path.join(path.normalize(__dirname), '../../uploads/', file));
            //     console.log(stats.mtime);
            //     console.log(stats.ctime);
            // })
        })
        // incomingForm.parse(req, function (err, fields, files) {
        //     if (!err) {
        //         console.log("Received file:", files.file);

        //         //proxy request

        //         // console.log("Appending file to be proxied")
        //         // formdata.append('file', fs.createReadStream(path.join(__dirname, "image.png"))));

        //         // xhr.onreadystatechange = function () {
        //         //     console.log("Proxy State: " + this.readyState);

        //         //     if (this.readyState === 4) {
        //         //         console.log("Complete.\nBody length: " + this.responseText.length);
        //         //         console.log("Body:\n" + this.responseText);
        //         //         console.log("Returning response from ML...");
        //         //         res.status(200).send(this.responseText);
        //         //     }
        //         // };

        //         // xhr.open("POST", configuration.mlUrl, true);
        //         // xhr.send(formdata);
        //         // console.log("proxing...");
        //     }else{
        //         res.status(500).send("UPSI");
        //     }

        // })




    }
}
