/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var account = require('../handlers/tables/h_account');

passport.use(new LocalStrategy({
    usernameField: 'username'
  },
  function(username, password, done) {
    account.getbyUserName(username, function(e, user){
      if(e){
        return done(e);
      }
      if(!user){
        return done(null, false, {message: 'Account not found'})
      }
      if(!account.validPassword(user, password)){
        return done(null, false, {message: 'Password is wrong'})
      }
      //do not return the hash and salt
      return done(null, {id: user.idaccounts, username: user.username, idroles: user.idroles});
    })
  }
));