/*made by Maryam Berenji @ KBZ maryamberenji@yahoo.com*/

/* HANLDER ACCOUNT DB*/

var configuration = require('../../../config.json');
var logger = require('../../../Config/logger.js');

var request = require('request');


//Authentication methods
var jwt = require('jsonwebtoken');
var crypto = require('crypto');

var setSalt = function(password){
	return crypto.randomBytes(16).toString('hex');
}
var setHash = function(salt, password){
	return crypto.pbkdf2Sync(password, salt, 1000, 16, 'sha512').toString('hex');
}

var _generateJwt = function(user){
	var expiry = new Date();
	expiry.setDate(expiry.getDate() + 7);
	let idAccounts
	if (user.id){
		idAccounts = user.id
	}else{
		idAccounts = 0
	}
	return jwt.sign({
		id: idAccounts,
		username: user.username,
		role: user.idroles,
		exp: parseInt(expiry.getTime() / 1000)
	}, "AWESOME_SECRET");
}


/* CRUD */

module.exports = {
	generateJwt: function(user){
		return _generateJwt(user);
	},
	validPassword : function(user, password){
		return user.userhash === crypto.pbkdf2Sync(password, user.usersalt, 1000, 16, 'sha512').toString('hex')
	},
	create : function(user, password, cb){
		logger.info("h_account:create");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + "/tables/accounts/rows?filter=userName="+"'"+user+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					result = json.list_of_rows
					if(result.length > 0){
						cb(false,"Email already Exists");
					}else{
						//Auth
						userSalt = setSalt(password);
						userHash = setHash(userSalt, password);
						var message = [{
							userName: user,
							userHash: userHash,
							userSalt: userSalt,
							idRoles: 1
						}]
						var Requestoptions = {
							url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows',
							method: 'POST',
							headers: {
								"Content-Type": configuration.RelationalStorage.ContentType,
								"Accept": configuration.RelationalStorage.Accept,
								"Authorization": configuration.RelationalStorage.Authorization
							},
							body: JSON.stringify(message)
						}
						request(Requestoptions, function (error, response, body) {
							console.log("Received response: " + JSON.stringify(response));
							if (!error) {	
								if(response.statusCode == 201){
									//Generate jwt
									cb(false, _generateJwt({username: user, idroles: 3}))
								}else{
									json = JSON.parse(response.body);
									cb(false,json.message);
								}
							}else{
								cb(true,"Relational Storage Enabler not responding");
							}
						})
					}
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for deleting specific account by UserName
	in: 
		userName: string
	out: 
		error: true/false, 
		response: JSON
	*/
	delete : function(accountID, cb){
		logger.info("h_account:delete");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows?filter=idAccounts='+"'"+accountID+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete Account: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for updating username on specific account by ID
	in: 
		id: integer (identify Account ID), 
		newUserName: string
	out: 
		error: true/false, 
		response: JSON
	*/
	updateNamebyID : function (id, newUserName, cb) {
		logger.info("h_account:updateNamebyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + "/tables/accounts/rows?filter=userName="+"'"+newUserName+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			// console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					result = json.list_of_rows;
					if(result.length > 0){
						cb(false,"Email already Exists");
					}else{
						var message = {
							username: newUserName
						};
						var Requestoptions = {
							url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows?filter=idAccounts='+"'"+id+"'",
							method: 'PATCH',
							headers: {
								"Content-Type": configuration.RelationalStorage.ContentType,
								"Accept": configuration.RelationalStorage.Accept,
								"Authorization": configuration.RelationalStorage.Authorization
							},
							body: JSON.stringify(message)
						}
						request(Requestoptions, function (error, response,body) {
							console.log("Received response: " + JSON.stringify(response));
							if (!error) {	
								if(response.statusCode == 200){
									cb(false,"Update Name on Account: Successfully");
								}else{
									json = JSON.parse(response.body);
									cb(false,json.message);
								}
							}else{
								cb(true,"Relational Storage Enabler not responding");
							}
						})
					}
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	updateRolebyID : function (id, roleID, cb) {
		logger.info("h_account:updateRolebyID");
		var message = {
			idroles: roleID
		};
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows?filter=idAccounts='+"'"+id+"'",
			method: 'PATCH',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Update Role on Account: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for updating hash and salt on specific account by ID
	in: 
		id: integer (identify Account ID), 
		newHash: string, 
		newSalt: string
	out: 
		error: true/false, 
		response: JSON
	*/
	updatePassword : function (id, pwd, cb){
		logger.info("h_account:updatePassword");
		salt = setSalt(pwd);
		hash = setHash(salt, pwd);
		var message = {
			userHash: hash,
			userSalt: salt
		};
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows?filter=idAccounts='+"'"+id+"'",
			method: 'PATCH',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Update Password on Account: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get All account
	in: 
		none
	out: 
		error: true/false, 
		response: JSON
	*/
	getAll : function (cb){
		logger.info("h_account:getAll");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows',
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		};
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	getAllAccounts : function (id, cb){
		logger.info("h_account:getAllAccounts");
		var accounts = []
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows',
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					accountsAll = json.list_of_rows
					accountsAll.forEach(function(eachAccount, accountIndex){
						if(eachAccount.idaccounts != id){
							accounts.push({eachAccount})
						}
					})
					cb(false,accounts);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get account by ID
	in: 
		id: integer (identify Account ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	getbyID : function (id, cb){
		logger.info("h_account:getbyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/accounts/rows?filter=idAccounts='+"'"+id+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get account by Name
	in: 
		username: string
	out: 
		error: true/false, 
		response: JSON
	*/
	getbyUserName : function (username, cb){
		logger.info("h_account:getbyUserName");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + "/tables/accounts/rows?filter=userName="+"'"+username+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows[0]);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	}
}

