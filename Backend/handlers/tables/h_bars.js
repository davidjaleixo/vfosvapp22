/*made by KBZ */
    
var account = require ('./h_account.js');
var configuration = require('../../../config.json');
var request = require('request');
var logger = require('../../../Config/logger.js');



module.exports = {
	create:function(bartype, barprojectid,barphoto,barphotoTransport,barphotoMarkings, cb){
		logger.info("h_bars:create");
		console.log("creating a new bar...");
		
		var message = [{
			type: bartype,
			idprojects : barprojectid,
			photo : barphoto,
			photoTransport : barphotoTransport, 
			photoMarkings : barphotoMarkings
		}];

		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/bars/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)

		}
		request(Requestoptions, function (error, response, body) {
			logger.info("h_bars:response from storage:"+JSON.stringify(response))
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"Tag inserted: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	delete : function (bartype, barprojectid, cb) {
		logger.info("h_bars:delete");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/bars/rows?filter=type='+"'"+bartype+"'"+'and idprojects='+"'"+barprojectid+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			// console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Bar removed from the project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	get : function(pid, cb){
        logger.info("h_bars:getByProjectId");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/bars/rows?filter='+'idprojects='+pid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
        }
		request(Requestoptions, function (error, response, body) {
            // logger.info("h_bars response: ",response);
			if (!error) {
				json = JSON.parse(response.body);
				cb(false,JSON.parse(response.body).list_of_rows)
			}else{
				json = JSON.parse(response.body);
				cb(false, json.message);
			}
		})
	}
}
