/*made by KBZ */
    
var account = require ('./h_account.js');
var configuration = require('../../../config.json');
var request = require('request');
var logger = require('../../../Config/logger.js');


module.exports = {
	create:function(bartestprojectid,bartestapproved,bartestdetected,bartestok,bartestdatetime,photoTransport, cb){
		logger.info("h_barsTest:create");
		var message = [{
			idproject : bartestprojectid,
            approved : bartestapproved,
            detected : bartestdetected,
            testok : bartestok,
			datetime : bartestdatetime,
			photoTransport: photoTransport
		}];
console.log(bartestok);
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/barstest/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)

		}
		request(Requestoptions, function (error, response, body) {
			logger.info("h_barstest:response from storage:"+JSON.stringify(response))
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"SteelID test inserted: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	get : function(pid, cb){
        logger.info("h_barstest:getByProjectId");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/barstest/rows?filter='+'idproject='+pid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
        }
		request(Requestoptions, function (error, response, body) {
            logger.info("h_barstest response: ",response);
			if (!error) {
				json = JSON.parse(response.body);
				cb(false,JSON.parse(response.body).list_of_rows)
			}else{
				json = JSON.parse(response.body);
				cb(false, json.message);
			}
		})
	}
}

/*
{
	"columns": [
		{
			"default": null,
			"name": "idproject",
			"type": "integer",
			"size": 0,
			"allow_null": false
		},
        {
			"default": "No",
			"name": "approved",
			"type": "varchar",
			"size": 10,
			"allow_null": false
		},
        {
			"default": "",
			"name": "detected",
			"type": "varchar",
			"size": 20,
			"allow_null": true
		},
        {
			"default": "Not OK",
			"name": "testok",
			"type": "varchar",
			"size": 6,
			"allow_null": true
		},
        {
			"default": null,
			"name": "datetime",
			"type": "text",
			"size": 0,
			"allow_null": true
		},
		{
			"default": null,
			"name": "photoTransport",
			"type": "text",
			"size": 0,
			"allow_null": true
		}
	],
	"table_name": "barstest"
}
*/