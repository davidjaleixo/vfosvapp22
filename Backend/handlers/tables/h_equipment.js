/*made by Thrinisha Mohandas @ KBZ thrinisha.mohandas@knowledgebiz.pt*/

/* HANLDER EQUIPMENT DB*/

var configuration = require('../../../config.json');
var logger = require('../../../Config/logger.js');

var request = require('request');

/* CRUD */

module.exports = {

	/*
	description: Handler for creating equipment
	in: 
		name: string, 
		idProjects: integer (identify Project ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	create : function (name, idProjects, cb) {
		logger.info("h_equipment:create");
		var message = [{
			name: name,
			idProjects: idProjects
		}];
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/equipments/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"Create Equipment: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for deleting specific equipment
	in: 
		id: integer (identify Equipment ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	delete : function (id, cb) {
		logger.info("h_equipment:delete");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/equipments/rows?filter=idEquipments='+"'"+id+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete Equipment: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get equipment by ID
	in: 
		id: integer (identify Equipment ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	getbyID : function (id, cb){
		logger.info("h_equipment:getbyID");
	var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/equipments/rows?filter=idEquipments='+"'"+id+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	getbyProjectID:function(pid, cb){
		logger.info("h_equipment:getbyProjectID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/equipments/rows?filter=idprojects='+pid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response, body) {
			logger.debug(response);
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	}
}

