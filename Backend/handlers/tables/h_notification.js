/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

var configuration = require('../../../config.json');
var logger = require('../../../Config/logger.js');

var request = require('request');

module.exports = {
	create:function(accList, stid, date, cb){
		logger.info("h_notification:create");
		var message = [];
		accList.forEach(function(eachAcc, idx, array){
			message.push(
			{
				idaccounts: eachAcc,
				idslumptests: stid,
				date: date,
				read: false
			})
			if(idx == array.length -1){
				var Requestoptions = {
					url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/notifications/rows',
					method: 'POST',
					headers: {
						"Content-Type": configuration.RelationalStorage.ContentType,
						"Accept": configuration.RelationalStorage.Accept,
						"Authorization": configuration.RelationalStorage.Authorization
					},
					body: JSON.stringify(message)
				}
				request(Requestoptions, function (error, response,body) {
					logger.debug("h_notification:create", response);
					if (!error) {	
						if(response.statusCode == 201){
							cb(false,"Create Notification: Successfully");
						}else{
							json = JSON.parse(response.body);
							cb(false,json.message);
						}
					}else{
						cb(true,"Relational Storage Enabler not responding");
					}
				})
			}
		})
	},
	update: function (body, cb) {
		logger.info("h_notification:update");
		var message = null;
		for (var i = 0; i <= body.length - 1; i++) {
			var readCheck = false;
			if(body[i].read == "t"){
				readCheck = true;
			}else{
				readCheck = false;
			}
			var message = {
				read: readCheck
			};
			var Requestoptions = {
				url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/notifications/rows?filter=idnotification='+"'"+body[i].idnotification+"'",
				method: 'PATCH',
				headers: {
					"Content-Type": configuration.RelationalStorage.ContentType,
					"Accept": configuration.RelationalStorage.Accept,
					"Authorization": configuration.RelationalStorage.Authorization
				},
				body: JSON.stringify(message)
			}
			request(Requestoptions, function (error, response,body) {
				// console.log("Received response: " + JSON.stringify(response));
				if (error) {	
					cb(true,"Relational Storage Enabler not responding");
				}else{
					if(response.statusCode == 200){
						message = "Update Notifications State: Successfully"
					}else{
						message = json.message;
					}
				}
			})
		}
		cb(false, message);
	},
	getByAccId:function(accid, cb){
		logger.info("h_notification:getByAccId");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/notifications/rows?filter=idaccounts='+accid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			logger.debug("h_notification:create", response);
			if (!error) {	
				json = JSON.parse(response.body);
				cb(false,json.list_of_rows);
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	getBySlumptestID:function(sid, cb){
		logger.info("h_notification:getBySlumptestID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/notifications/rows?filter=idSlumptests='+sid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			logger.debug("h_notification:create", response);
			if (!error) {	
				json = JSON.parse(response.body);
				if(json.list_of_rows.length == 0){
					cb(false,false);
				}else{
					cb(false,true);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	}
}
