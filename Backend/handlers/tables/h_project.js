/*made by Thrinisha Mohandas @ KBZ thrinisha.mohandas@knowledgebiz.pt*/

/* HANLDER PROJECT DB*/

var configuration = require('../../../config.json');
var logger = require('../../../Config/logger.js');

var request = require('request');


/* CRUD */

module.exports = {

	/*
	description: Handler for creating project
	in: 
		name: string, 
		description: string,
		status: true/false,
		idAuthorization: integer
	out: 
		error: true/false, 
		response: JSON
	*/
	create : function (name, description,cb) {
		logger.info("h_project:create");
		var message = [{
			name: name,
			description: description,
			status: true
		}];
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"Create Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for deleting specific project by ID
	in: 
		id: integer (identify Project ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	deletebyID : function (id, cb) {
		logger.info("h_project:deletebyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idProjects='+"'"+id+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for deleting projects by Status
	in: 
		status: boolean
	out: 
		error: true/false, 
		response: JSON
	*/
	deletebyStatus : function (status, cb) {
		logger.info("h_project:deletebyStatus");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=status='+"'"+status+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for updating name for specific project by ID
	in: 
		id: integer (identify Project ID), 
		name: string
	out: 
		error: true/false, 
		response: JSON
	*/
	updateNamebyID: function (id, newname, cb) {
		logger.info("h_project:updateNamebyID");
        var message = {
			name: newname
		};
		console.log("new name is::::::::::"+JSON.stringify(message))
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idProjects='+id,
			method: 'PATCH',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Update Name of Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	updateTholdbyID: function (id, thold, cb) {
		logger.info("h_project:updateTholdbyID");
        var message = {
			threshold: thold
		};
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idProjects='+id,
			method: 'PATCH',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Update Name of Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for updating description for specific project by ID
	in: 
		id: integer (identify Project ID),  
		description: string
	out: 
		error: true/false, 
		response: JSON
	*/
	updateDescriptionbyID: function (id, newdescription, cb) {
		logger.info("h_project:updateDescriptionbyID");
        var message = {
			description: newdescription
		};
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idProjects='+id,
			method: 'PATCH',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Update Description of Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for updating status for specific project by ID
	in: 
		id: integer (identify Project ID), 
		status: true/false
	out: 
		error: true/false, 
		response: JSON
	*/
	updateStatusbyID: function (id, newstatus, cb) {
		logger.info("h_project:updateStatusbyID");
        var message = {
			status: newstatus
		};
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idProjects='+id,
			method: 'PATCH',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			logger.debug("h_project:updateStatusbyID: response: " + JSON.stringify(response))
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Update Status of Project: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get project by ID
	in: 
		id: integer (identify Project ID)
	out: 
		error: true/false, 
		response: JSON
	*/
    getbyID : function (id , cb){
    	logger.info("h_project:getbyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idprojects='+id,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			//console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows[0]);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get projects by Name
	in: 
		name: string
	out: 
		error: true/false, 
		response: JSON
	*/
    getbyName : function (name, cb){
    	logger.info("h_project:getbyName");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=name='+"'"+name+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows[0]);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get projects by Status
	in: 
		status: boolean 
	out: 
		error: true/false, 
		response: JSON
	*/
	getbystatus : function (status, cb){
		logger.info("h_project:getbystatus");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=status='+"'"+status+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	[DA]
	description: Filter the projects by the account id
	in: 
		accid: Account identification as String 
	out: 
		error: true/false, 
		response: JSON containing 
	*/
	getByAccountId : function(accid, cb){
		logger.info("h_project:getByAccountId");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/projects/rows?filter=idaccounts='+"'"+accid+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response, body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	}
}

