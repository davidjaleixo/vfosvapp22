/*made by Maryam Berenji @ KBZ maryamberenji@yahoo.com*/

/* HANLDER ROLE DB*/

var configuration = require('../../../config.json');
var logger = require('../../../Config/logger.js');

var request = require('request');

/* CRUD */

module.exports = {

	/*
	description: Handler for creating role
	in: 
		accountType: string
	out: 
		error: true/false, 
		response: JSON
	*/
	create : function (accountType, description, cb) {
		logger.info("h_role:create");
		var message = [{
			accountType: accountType,
			description: description
		}];
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/roles/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"Create Authorization: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for deleting specific role by ID
	in: 
		id: integer (identify Role ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	delete : function (id, cb){
		logger.info("h_role:delete");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/roles/rows?filter=idRoles='+"'"+id+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete Authorization: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get All roles
	in: 
		none
	out: 
		error: true/false, 
		response: JSON
	*/
	get : function (cb){
		logger.info("h_role:get");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/roles/rows',
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			if (!error) {
				json = JSON.parse(response.body);
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	}
}

