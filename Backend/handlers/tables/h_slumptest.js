/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

/* HANLDER SLUMPTEST DB*/

var notification = require ('./h_notification.js');
var configuration = require('../../../config.json');
var logger = require('../../../Config/logger.js');

var request = require('request');

/* CRUD */

module.exports = {

	/*
	description: Handler for creating threshold
	in: 
		value_slumptest: integer, 
		idProjects: integer (identify Project ID), 
		idEquipments: integer (identify Equipment ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	create : function (value, idEquipments, idProjects, idaccounts, cb) {
		logger.info("h_slumptest:create");
		var utcDate = new Date().toUTCString();
		var message = [{
			value: value,
			date: utcDate,
			idEquipments: idEquipments,
			idProjects: idProjects,
			idAccounts: idaccounts
		}];
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/slumptests/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"Create SlumpTest: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	delete : function (id, cb){
		logger.info("h_slumptest:delete");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/slumptests/rows?filter=idProjects='+"'"+id+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete SlumpTest: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get slumptest by ID
	in: 
		id: idSlumptests: integer (identify Slumptest ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	getbyID : function (idSlumptests, cb) {
		logger.info("h_slumptest:getbyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/slumptests/rows?filter=idslumptests='+idSlumptests,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			logger.info("h_slumptest:getbyID: response:");
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows[0]);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for get slumptests for specific Project ID
	in: 
		id: idThresholds: integer (identify Threshold ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	getbyProjectIDHistory : function (idProjects, cb) {
		logger.info("h_slumptest:getbyProjectIDHistory");
		var noNotification = 0;
		var not = 0;
		var results = [];
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/slumptests/rows?filter=idProjects='+"'"+idProjects+"'",
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					res = json.list_of_rows
					res.forEach(function(eachSlumptest, slumptestIndex){
						notification.getBySlumptestID(eachSlumptest.idslumptests, function(e, result){
							if(!e){
								if(result == true){
									not = not + 1
								}else{
									noNotification = noNotification + 1;
								}
								if(res.length ==  noNotification + not){
									results.push(res, {not: not, nonot: noNotification})
									cb(false, results)
								}
							}else{
								cb(true,"Relational Storage Enabler not responding");
							}
						})
					})
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	getLastbyAccountID : function (accid, cb) {
		logger.info("h_slumptest:getLastbyAccountID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/slumptests/rows?filter=idaccounts='+accid+'&order_by=idaccounts',
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					json = JSON.parse(response.body);
					cb(false,json.list_of_rows[json.list_of_rows.length-1]);
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	}
}