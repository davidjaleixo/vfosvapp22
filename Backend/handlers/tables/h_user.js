/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

var account = require ('./h_account.js');
var configuration = require('../../../config.json');
var request = require('request');
var logger = require('../../../Config/logger.js');


module.exports = {
	create:function(accid, projectid, cb){
		logger.info("h_user:create");
		var message = [{
			idprojects: projectid,
			idaccounts : accid
		}];

		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/users/rows',
			method: 'POST',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			},
			body: JSON.stringify(message)

		}
		request(Requestoptions, function (error, response, body) {
			logger.info("h_user:response from storage:"+JSON.stringify(response))
			if (!error) {	
				if(response.statusCode == 201){
					cb(false,"Create users: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	/*
	description: Handler for deleting specific user
	in: 
		id: integer (identify user ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	deletebyID : function (accountID, projectID, cb) {
		logger.info("h_user:deletebyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/users/rows?filter=idaccounts='+"'"+accountID+"'"+'and idprojects='+"'"+projectID+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete User: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
		/*
	description: Handler for deleting specific user by ProjectID
	in: 
		projectID: integer (identify Project ID)
	out: 
		error: true/false, 
		response: JSON
	*/
	deletebyProjectID : function (projectID, cb) {
		logger.info("h_user:deletebyID");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/users/rows?filter=idprojects='+"'"+projectID+"'",
			method: 'DELETE',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response,body) {
			console.log("Received response: " + JSON.stringify(response));
			if (!error) {	
				if(response.statusCode == 200){
					cb(false,"Delete User: Successfully");
				}else{
					json = JSON.parse(response.body);
					cb(false,json.message);
				}
			}else{
				cb(true,"Relational Storage Enabler not responding");
			}
		})
	},
	getByAccountId : function(accid,cb){
		logger.info("h_user:getByAccountId");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/users/rows?filter='+'idAccounts='+accid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response, body) {
			logger.info("h_user response: ",response);
			if (!error) {	
				json = JSON.parse(response.body);
				cb(false,json.list_of_rows)
			}else{
				json = JSON.parse(response.body);
				cb(false, json.message);
			}
		})
	},
	getByProjectId : function(pid, cb){
		logger.info("h_user:getByProjectId");
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/users/rows?filter='+'idprojects='+pid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response, body) {
			logger.info("h_user response: ",response);
			if (!error) {
				json = JSON.parse(response.body);
				cb(false,JSON.parse(response.body).list_of_rows)
			}else{
				json = JSON.parse(response.body);
				cb(false, json.message);
			}
		})
	}, 
	getUsersNotOnProject : function(pid, cb){
		logger.info("h_user:getUsersNotOnProject");
		var accounts = []
		var bool = false
		var Requestoptions = {
			url: configuration.RelationalStorage.URL + '/databases/' + configuration.RelationalStorage.databaseName + '/tables/users/rows?filter='+'idprojects='+pid,
			method: 'GET',
			headers: {
				"Content-Type": configuration.RelationalStorage.ContentType,
				"Accept": configuration.RelationalStorage.Accept,
				"Authorization": configuration.RelationalStorage.Authorization
			}
		}
		request(Requestoptions, function (error, response, body) {
			if (!error) {
				json = JSON.parse(response.body);
				resultGetUserbyProjectID = JSON.parse(response.body).list_of_rows
				account.getAll(function(e, result){
					if(!e){
						result.forEach(function(eachAccount, accountIndex){
							bool = false
							resultGetUserbyProjectID.forEach(function(eachAccountonProject, accountProjectIndex){
								if(eachAccount.idaccounts != eachAccountonProject.idaccounts){
									if(accountProjectIndex == resultGetUserbyProjectID.length - 1){
										if(bool == false){
											accounts.push({eachAccount})
										}
									}
								}else{
									bool = true
								}
							})	
							if(accountIndex == result.length- 1){
								cb(false,accounts)
							}
						})
					}
				})
			}else{
				json = JSON.parse(response.body);
				cb(false, json.message);
			}
		})
	}
}