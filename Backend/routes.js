/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

const multipart = require('connect-multiparty');
const multipartMiddleware = multipart({
    uploadDir: './uploads'
});

//routes.js

var constants = require('../constants.js');
var ctl_account = require('./Controllers/ctl_account.js');
var ctl_role = require('./Controllers/ctl_role.js');
var ctl_equipment = require('./Controllers/ctl_equipment.js');
var ctl_project = require('./Controllers/ctl_project.js');
var ctl_slumptest = require('./Controllers/ctl_slumptest.js');
var ctl_notification = require('./Controllers/ctl_notification.js');
var ctl_main = require('./Controllers/ctl_initialization');
var ctl_users = require('./Controllers/ctl_users');
var ctl_steelbartest = require('./Controllers/ctl_steelbar');
var ctl_steelbartestHistory = require('./Controllers/ctl_steelbartestshistory');
var ctl_proxy = require('./Controllers/proxy');



//authentication controller
var auth = require('./Controllers/auth');
var jwt = require('express-jwt');
var authorization = jwt({
	secret: 'AWESOME_SECRET',
	userProperty: 'payload'
});


module.exports = function(server) {

    // Server routes to FrontEnd ===========================================================

	//Init
	server.get('/api/vf-os-pilot2-vApp4/init', ctl_main.init);

    //Authentication
    server.post('/api/vf-os-pilot2-vApp4/login',auth.login);
    server.post('/api/vf-os-pilot2-vApp4/register', auth.register);

	//Role
	server.post('/api/vf-os-pilot2-vApp4/roles', ctl_role.create);
	server.delete('/api/vf-os-pilot2-vApp4/roles', ctl_role.delete);
	server.get('/api/vf-os-pilot2-vApp4/roles', ctl_role.get);

	//Account
	server.post('/api/vf-os-pilot2-vApp4/accounts', ctl_account.create); 
	server.delete('/api/vf-os-pilot2-vApp4/accounts/:accountID', ctl_account.delete);
	server.patch('/api/vf-os-pilot2-vApp4/accounts/updateName', ctl_account.updateName);
	server.patch('/api/vf-os-pilot2-vApp4/accounts/updateRole', ctl_account.updateRole);
	server.patch('/api/vf-os-pilot2-vApp4/accounts/updatePassword', ctl_account.updatePassword);
	server.get('/api/vf-os-pilot2-vApp4/accounts/getbyID/:accountID', ctl_account.getbyID);
	server.get('/api/vf-os-pilot2-vApp4/accounts/getbyUserName/:userName', ctl_account.getbyUserName);
	server.get('/api/vf-os-pilot2-vApp4/accounts/getAllAccounts', ctl_account.getAllAccounts);

	//Project
	server.post('/api/vf-os-pilot2-vApp4/projects', ctl_project.create);
	server.delete('/api/vf-os-pilot2-vApp4/projects', ctl_project.delete);
	server.patch('/api/vf-os-pilot2-vApp4/projects/updateName', ctl_project.updateName);
	server.patch('/api/vf-os-pilot2-vApp4/projects/updateDescription', ctl_project.updateDescription);
	server.patch('/api/vf-os-pilot2-vApp4/projects/updateStatus', ctl_project.updateStatus);
	server.patch('/api/vf-os-pilot2-vApp4/projects/updateThold', ctl_project.updateThold);
	server.get('/api/vf-os-pilot2-vApp4/projects', authorization, ctl_project.get);
	server.get('/api/vf-os-pilot2-vApp4/projects/get', ctl_project.getbyID);

	//users
	server.post('/api/vf-os-pilot2-vApp4/users', ctl_users.create);
	server.get('/api/vf-os-pilot2-vApp4/users/:projectid', ctl_users.get);
	server.get('/api/vf-os-pilot2-vApp4/users/getbyNOTPrjctID/:projectid', ctl_users.getUsersNotOnProject);
	server.delete('/api/vf-os-pilot2-vApp4/users/:accountID/:projectID', ctl_users.delete);

	//Equipment
	server.post('/api/vf-os-pilot2-vApp4/equipments', ctl_equipment.create);
	server.delete('/api/vf-os-pilot2-vApp4/equipments', ctl_equipment.delete);
	server.get('/api/vf-os-pilot2-vApp4/equipments', ctl_equipment.get);

	//Notifications
	server.get('/api/vf-os-pilot2-vApp4/notifications', ctl_notification.get);
	server.patch('/api/vf-os-pilot2-vApp4/notifications', ctl_notification.update);

	//Slumptest
	server.post('/api/vf-os-pilot2-vApp4/slumptests', ctl_slumptest.create, ctl_notification.validate);
	server.get('/api/vf-os-pilot2-vApp4/slumptests', ctl_slumptest.get);

	//Steelbartest
	server.post  ('/api/vf-os-pilot2-vApp2/tags', ctl_steelbartest.create, ctl_notification.validate);
	server.get   ('/api/vf-os-pilot2-vApp2/tags/:projectid', ctl_steelbartest.get);
	server.delete('/api/vf-os-pilot2-vApp2/tags/:tagType/:projectID', ctl_steelbartest.delete);

	//SteelbartestHistory
	server.post  ('/api/vf-os-pilot2-vApp2/tagshistory', ctl_steelbartestHistory.create, ctl_notification.validate);
	server.get   ('/api/vf-os-pilot2-vApp2/tagshistory/:projectid', ctl_steelbartestHistory.get);
	
	//proxy
	server.all ('/api/vf-os-pilot2-vApp2/proxy', multipartMiddleware, ctl_proxy.proxyIt);



	server.get('*', function(req, res) {
        res.sendFile('index.html', { root: constants.FRONTEND_PATH + '/public/views/' }); // load our public/index.html file
    });	
}