/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

//server.js

var express = require('express');
var server = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');

//Authentication
var passport = require('passport');

var constants = require('../constants.js');
var configuration = require('../config.json');
var logger = require('../Config/logger.js');

var port = process.env.PORT || configuration.Vapp.PORT;

//Server Use
// server.use(bodyParser.json());

server.use(bodyParser.json({ limit: '50mb' }))

//update the limits for the payload
// server.use(bodyParser.json({limit: '50mb'}));
server.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// server.use(bodyParser.urlencoded({ extended: true }));

server.use(methodOverride('X-HTTP-Method-Override'));

server.use(express.static(path.join(path.normalize(__dirname), '../Frontend/public')));


//include the authentication mechanism
require('./config/passport.js');
server.use(passport.initialize());

require('./Controllers/ctl_initialization').init();

//Routes
require('./routes')(server);

server.listen(port);
logger.info("VF Steel Validation running internally at port:" + port);

//Expose server
exports = module.exports = server