/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

//logger.js

var winston = require('winston');
var configuration = require('../config.json')

module.exports = mylogger = winston.createLogger({
	level: configuration.Logger.DEBUGLEVEL,
	transports: [
      new (winston.transports.Console)(),
      new (winston.transports.File)({ filename: 'vfconcretefeedback.log' })
    ]
});

