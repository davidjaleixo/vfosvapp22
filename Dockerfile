#KBZ @ vf-OS project
#Knowledgebiz Team
# Dockerfile for a node container

#nodejs
FROM node:alpine

ENV ASSET_NAME="steelvalidation"

# app directory
RUN mkdir -p /usr/src/app
# this lets the working directory for every COPY RUN and CMD command
WORKDIR /usr/src/app

COPY . .

# get the node package file
# wildcard used to ensure both package.json and package-lock.json are copied
# COPY package*.json /usr/src/app/
# COPY bower.json /usr/src/app/
# COPY .bowerrc /usr/src/app/

# install dependencies
RUN apk add --no-cache git
RUN npm install -g bower
RUN apk add --no-cache --virtual .gyp \
python \
make \
g++
RUN npm install
RUN bower install --allow-root

RUN apk add --no-cache bash

# Expose the app port
EXPOSE 4201

LABEL vf-OS=true
LABEL vf-OS.icon=img/2.png
LABEL vf-OS.urlprefixReplace=true
LABEL vf-OS.frontendUri="/steelvalidation"
LABEL vf-OS.name=steelvalidation
LABEL vf-OS.boxshot="https://knowledgebiz.sharepoint.com/:i:/g/EbastEf2mDJMjPsYmbRqJQgBnFItf2Hyawnc9KkVGlpNvQ?e=Kh2hcV"

CMD [ "npm", "start" ]
