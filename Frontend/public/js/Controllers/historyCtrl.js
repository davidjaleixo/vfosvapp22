/*made by Thrinisha Mohandas @ KBZ thrinisha.mohandas@knowledgebiz.pt*/


angular.module('mainApp').controller('historyCtrl', function ($scope, $routeParams, $http, projectService, equipmentsService, slumptestService, $timeout) {
	console.log("Running historyCtrl... ");
	$scope.projectId = $routeParams.idprojects;
	$scope.show = false;
	$scope.showError = false;
	$scope.hideTableCheck = false;
	$scope.tableContent = false;
	$scope.showEquipment = false;
	$scope.showSelectEquipment = true;

	$scope.checkExistEquip = function () {
		if ($scope.newST.equipment != null && $scope.newST.equipment != "Choose...") {
			// Validation failed
			$scope.showSelectEquipment = false;
		} else {
			$scope.showSelectEquipment = true;
		}
	}

	projectService.getProject($routeParams.idprojects).then(function (response) {
		$scope.projectName = response.data.Reason.name;
	});

	slumptestService.getSlumptest($scope.projectId).then(function (response) {
		console.log("getSlumptest: " + JSON.stringify(response));
		if (response.data.data.length == 0) {
			$scope.tableContent = false;
		} else {
			$scope.tableContent = true;
			// Load Google Charts
			google.charts.load('current', { 'packages': ['corechart'] });
			google.charts.setOnLoadCallback(drawChart);

			function drawChart() {
				var data = google.visualization.arrayToDataTable([
					['SlumpTests', 'Average'],
					['No Notification', response.data.data[1].nonot],
					['Notification', response.data.data[1].not],
				]);
				var options = { 'title': 'Average between Slumptests and Notifications', 'width': 500, 'height': 500 };
				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
				chart.draw(data, options);
			}
			$scope.values = response.data.data[0];
		}
	});
	equipmentsService.get($routeParams.idprojects).then(function (r) {
		if (r.data.Reason == 0) {
			$scope.showEquipment = false;
		} else {
			$scope.showEquipment = true;
			$scope.equipments = r.data.Reason;
		}
	})
	//New slump test modal variables
	$scope.newST = {
		value: null,
		equipment: null,
		project: $routeParams.idprojects,
		account: $scope.user.id
	};
	$scope.createST = function () {
		$scope.showSelectEquipment = false;
		slumptestService.create($scope.newST).then(function (r) {
			console.log("Response " + JSON.stringify(r));
			if (r.data == "Create Notification: Successfully") {
				$scope.showError = true;
				$scope.user.notifications.length = $scope.user.notifications.length + 1
				$timeout(function () {
					$scope.showError = false;
				}, 3000)
			} else {
				$scope.show = true;
				$timeout(function () {
					$scope.show = false;
				}, 3000)
			}
		})
	};
	$scope.showTable = function () {
		$scope.hideTableCheck = true;
	};
	$scope.hideTable = function () {
		$scope.hideTableCheck = false;
	};
	console.log("Loading historyCtrl");
})