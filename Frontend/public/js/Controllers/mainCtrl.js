/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/


angular.module('mainApp').controller('mainCtrl', function($scope, $routeParams, $http, authService, notificationService, $timeout) {
	
	$scope.show = false;
	$scope.showError = false;
	$scope.msg = null;
	$scope.credentials = {
		username : "",
		password : "" 
	}
	$scope.login=function(){
		authService.login($scope.credentials).then(function(data){
			authService.saveToken(data.data.token);
			$scope.show = true;
			$scope.msg = data.data.Reason;
			$timeout(function() {
				$scope.show = false;
				window.location.href = '/steelvalidation/';
			}, 2000)
		},function(e){
			console.log("Auth error: " + e);
			if(e.status == 401){
				$scope.showError = true;
				$scope.msg = e.data.message;
				$timeout(function() {
					$scope.showError = false;
				}, 2000)
			}
		})
	}
	$scope.logout=function(){
		authService.logout();
		window.location.href = '/steelvalidation/';
	}
	//check if the user is already loggedin
	if(authService.isLoggedIn()){
		$scope.user = authService.getUserDetails()
		//check if the user has unread notifications
		notificationService.get().then(function(data){
			$scope.user.notifications = data.data;
			console.log($scope.user.notifications)
		});
	};
	
})