/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/



angular.module('mainApp').controller('notificationCtrl', function ($scope, $routeParams, $http, projectService, slumptestService, notificationService) {
	console.log("Running notificationCtrl... ");
	$scope.changes = 0
	for (var i = 0; i <= $scope.user.notifications.length - 1; i++) {
		if ($scope.user.notifications[i].read == 'f') {
			$scope.user.notifications[i].style = 'table-danger'
		}

		$scope.user.notifications.sort(function (a, b) {
			return a.idslumptests.localeCompare(b.idslumptests);
		});
		//complement with project information
		slumptestService.get($scope.user.notifications[i].idslumptests).then(function (slumpTest) {
			$scope.user.notifications.forEach(function (eachNotification) {
				if (eachNotification.idslumptests == slumpTest.data.data.idslumptests) {
					projectService.getProject(slumpTest.data.data.idprojects).then(function (res) {
						if (res.data.Result == true) {
							eachNotification.projectid = slumpTest.data.data.idprojects;
							eachNotification.projectname = res.data.Reason.name;
							console.log("row: " + JSON.stringify(eachNotification) + "result:" + slumpTest.data.data.idprojects);
						}
					})
				}
			})
		})
	}
	$scope.markasread = function (notificationid) {
		$scope.user.notifications.forEach(function (eachNotification) {
			if (eachNotification.idnotification == notificationid) {
				eachNotification.read = 't';
				eachNotification.style = '';
				$scope.changes++;
			}
		})
	}
	$scope.markasunread = function (notificationid) {
		$scope.user.notifications.forEach(function (eachNotification) {
			if (eachNotification.idnotification == notificationid) {
				eachNotification.read = 'f';
				eachNotification.style = 'table-danger';
				$scope.changes--;
			}
		})
	}
	$scope.savechanges = function () {
		notificationService.update($scope.user.notifications).then(function (e, r) {
		});

	};
	console.log("Loading notificationCtrl");
})