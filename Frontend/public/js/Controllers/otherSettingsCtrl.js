/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/


angular.module('mainApp').controller('otherSettingsCtrl', function ($scope, $route, $routeParams, $http, authService, accountService, roleService, $timeout) {
	console.log("Running otherSettingsCtrl... ");
	$scope.idaccount = 0;
	$scope.show = false;
	$scope.showError = false;

	accountService.getAllAccountsOnProject($scope.user.id).then(function(data){
		$scope.accounts = data.data.data;
	})
	roleService.get().then(function (response) {
		$scope.roles = response.data.Reason;
	})
	$scope.accountNameSave = function () {
		accountService.editAccountName($scope.user.id, $scope.newAccountEmail).then(function (res) {
            //refresh the view
            $scope.user.username = $scope.newAccountEmail;
            if(e.data.Result == true){
            	$scope.show = true;
            	$scope.msg = e.data.Reason;
            }else{
            	$scope.showError = true;
            	$scope.msg = e.data.Reason; 
            }

            $timeout(function() {
            	$scope.show = false;
            	$scope.showError = false;
            	$route.reload();
            }, 3000)
        })
	}
	$scope.accountPasswordSave = function () {
		accountService.editAccountPassword($scope.user.id, $scope.newAccountPassword).then(function (res, data) {
			if(e.data.Result == true){
				$scope.show = true;
				$scope.msg = e.data.Reason;
			}else{
				$scope.showError = true;
				$scope.msg = e.data.Reason; 
			}

			$timeout(function() {
				$scope.show = false;
				$scope.showError = false;
				$route.reload();
			}, 3000)
		})
	}
	//Edite/Delete Account
	$scope.idCheck = function (accountID) {
		$scope.idaccount = accountID
	}
	//Edit Accounts
	$scope.editAccount = function () {
		if($scope.newAccountsEmail != null){
			accountService.editAccountName($scope.idaccount, $scope.newAccountsEmail).then(function (e, r) {
				if(e.data.Result == true && e.data.Reason != "Email already Exists"){
					$scope.show = true;
					$scope.msg = e.data.Reason;
				}else{
					$scope.showError = true;
					$scope.msg = e.data.Reason; 
				}
			})
		}
		if($scope.newAccountsRole.idroles != 0){
			accountService.editAccountRole($scope.idaccount, $scope.newAccountsRole.idroles).then(function (e, r) {
				if(e.data.Result == true){
					$scope.show = true;
					$scope.msg = e.data.Reason;
				}else{
					$scope.showError = true;
					$scope.msg = e.data.Reason; 
				}
			})
		}if($scope.newAccountsPassword != null){
			accountService.editAccountPassword($scope.idaccount, $scope.newAccountsPassword).then(function (e, r) {
				if(e.data.Result == true){
					$scope.show = true;
					$scope.msg = e.data.Reason;
				}else{
					$scope.showError = true;
					$scope.msg = e.data.Reason; 
				}
			})
		}
		$timeout(function() {
			$scope.show = false;
			$scope.showError = false;
			$route.reload();
		}, 3000)
	}
    //Delete Account
    $scope.deleteAccount = function () {
    	accountService.delete($scope.idaccount).then(function (e, r) {
    		console.log(r)
    		$route.reload();
    	})
    };
    console.log("Loading otherSettingsCtrl");
})
