/*made by Maryam Berenji @ KBZ maryamberenji@yahoo.com*/


angular.module('mainApp').controller('projectCtrl', function($scope, $routeParams,$route, $http, projectService) {
	console.log("Running ProjectCtrl... ");
	
	$scope.loadingprojects = true;

	$scope.checkNameFormat = function () {
        if ($scope.newProject.name.includes("\'")) {
            // Validation failed
            $scope.projectNameFormatTest = true;
        }else { 
            $scope.projectNameFormatTest = false;  
        }
    }
    $scope.checkDesFormat = function () {
        if ($scope.newProject.description.includes("\'")) {
            // Validation failed
            $scope.projectDescriptionFormatTest = true;
        }else { 
            $scope.projectDescriptionFormatTest = false;  
        } 
    }

	projectService.get().then(function(data){
		
		$scope.projectsList = data.data.Reason;
		$scope.loadingprojects = false;
		$scope.projectsList.forEach(function(eachProject){
			if(eachProject.status == 'f'){
				eachProject.status = false
			}else{
				eachProject.status = true
			}
		})
		$scope.loadingprojects = false;
	})
	$scope.newProject = {
		name: "",
		description: "",
		status: true
	}
	$scope.createProject = function(){
		projectService.create($scope.newProject).then(function(e,r){
			console.log(r)
			if(e){
				//notify the error
				console.log(e)
			}else{
				//push the new project to the existing list
				$scope.projects.push($scope.newProject);
			}
			$route.reload();
		})
	}
	$scope.activeprojects = function(item){
		if($scope.user.role == 1 ){
			if(item.status == true){
				return item
			}
		}
		if($scope.user.role == 2 ){
			return item
		}
		if($scope.user.role == 3 ){
			if(item.status == true){
				return item
			}
		}
	};
	console.log("Loading ProjectCtrl");
})