/*made by Maryam Berenji @ KBZ maryam.berenji@knowledgebiz.pt*/


angular.module('mainApp').controller('projectDetailCtrl', function ($scope, $routeParams, $http, projectService, equipmentsService, slumptestService, $timeout) {
	console.log("Running projectDetailCtrl... ");
	$scope.projectId = $routeParams.idprojects;
	$scope.show = false;
	$scope.showError = false;
	$scope.showNotification = false;
	$scope.showEquipment = false;
	$scope.showSelectEquipment = true;

	$scope.checkExistEquip = function () {
		if ($scope.newST.equipment != null && $scope.newST.equipment != "Choose...") {
			// Validation failed
			$scope.showSelectEquipment = false;
		} else {
			$scope.showSelectEquipment = true;
		}
	}

	projectService.getProject($routeParams.idprojects).then(function (response) {
		$scope.projectName = response.data.Reason.name;
	});

	equipmentsService.get($routeParams.idprojects).then(function (r) {
		if (r.data.Reason == 0) {
			$scope.showEquipment = false;
		} else {
			$scope.showEquipment = true;
			$scope.equipments = r.data.Reason;
		}
	})
	//New slump test modal variables
	$scope.newST = {
		value: null,
		equipment: null,
		project: $routeParams.idprojects,
		account: $scope.user.id
	}
	$scope.createST = function () {
		$scope.showSelectEquipment = false;
		slumptestService.create($scope.newST).then(function (r) {
			console.log("Response " + JSON.stringify(r));
			if (r.data == "Create Notification: Successfully") {
				$scope.showError = true;
				$scope.user.notifications.length = $scope.user.notifications.length + 1
				$timeout(function () {
					$scope.showError = false;
				}, 3000)
			}
			else if (r.data == "Notification Created but Alert not sent") {
				$scope.showNotification = true;
				$scope.user.notifications.length = $scope.user.notifications.length + 1
				$timeout(function () {
					$scope.showNotification = false;
				}, 3000)
			}
			else {
				$scope.show = true;
				$timeout(function () {
					$scope.show = false;
				}, 3000)
			}
		})
	};
	console.log("Loading projectDetailCtrl");
})

