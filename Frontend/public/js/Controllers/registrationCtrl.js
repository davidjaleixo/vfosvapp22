/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/


angular.module('mainApp').controller('registrationCtrl', function($scope, $routeParams, $http, authService, $timeout) {
	console.log("Running registrationCtrl... ");
	$scope.show = false;
	$scope.showError = false;
	$scope.msg = null;
	$scope.username = "test@test.pt";
	$scope.password = "test";
	$scope.c_password = "test";

	$scope.register = function(){
		//password must match
		if($scope.password == $scope.c_password && $scope.username != ""){
			console.log("pass:" + $scope.password);
			authService.register($scope.username, $scope.password).then(function(response){
				console.log("response");
				console.log(response.data.token);
				if(response.data.token == "Email already Exists"){
					$scope.showError = true;
					$scope.msg = response.data.token;
					$timeout(function() {
						$scope.showError = false;
					}, 2000)
				}else{
					$scope.show = true;
					$scope.msg = response.data.token;
					$timeout(function() {
						$scope.show = false;
						authService.saveToken(response.data.token);
						window.location.href = '/steelvalidation/';
					}, 2000)
				}
			})
		}
	};
	console.log("Loading registrationCtrl");
})