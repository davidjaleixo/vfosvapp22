/*made by Maryam Berenji @ KBZ maryam.berenji@knowledgebiz.pt*/


angular.module('mainApp').controller('settingProjectCtrl', function ($scope, $route, $routeParams, $http, projectService, equipmentsService, roleService, accountService, userService, tagService, fileReader, $timeout) {
    console.log("Running settingProjectCtrl... ");
    $scope.projectId = $routeParams.idprojects;
    $scope.myusers = [];
    $scope.equipments = [];
    $scope.ProjectTags = [];

    $scope.hugeimageSrcMarkings = false;
    $scope.hugeimageSrc = false;

    $scope.loadingbars = true;
    $scope.loadingusers = true;

    $scope.david = { grande: 'pequeno' }

    $scope.checkNameFormat = function () {
        if ($scope.newProjectName.includes("\'")) {
            // Validation failed
            $scope.newProjectNameFormatTest = true;
        } else {
            $scope.newProjectNameFormatTest = false;
        }
    }
    $scope.checkDesFormat = function () {
        if ($scope.newProjectDescription.includes("\'")) {
            // Validation failed
            $scope.newProjectDescriptionFormatTest = true;
        } else {
            $scope.newProjectDescriptionFormatTest = false;
        }
    }
    $scope.checkEquipFormat = function () {
        if ($scope.newEquipmentName.includes("\'")) {
            // Validation failed
            $scope.projectEquipmentFormatTest = true;
        } else {
            $scope.projectEquipmentFormatTest = false;
        }
    }

    $scope.idCheck = function (accountID, userName) {
        $scope.idaccount = accountID
        $scope.newRole = userName
    }
    //Edit Accounts
    $scope.editAccount = function () {

        if ($scope.newAccountsRole.idroles != 0) {
            accountService.editAccountRole($scope.idaccount, $scope.newAccountsRole.idroles).then(function (e, r) {
                if (e.data.Result == true) {
                    $scope.show = true;
                    $scope.msg = e.data.Reason;
                } else {
                    $scope.showError = true;
                    $scope.msg = e.data.Reason;
                }
            })
        }
        $timeout(function () {
            $scope.show = false;
            $scope.showError = false;
            $route.reload();
        }, 3000)
    }
    roleService.get().then(function (response) {
        $scope.roles = response.data.Reason;
    })
    ////////
    projectService.getProject($routeParams.idprojects).then(function (response) {
        console.log(response.data);
        $scope.projectName = response.data.Reason.name;
        $scope.projectId = response.data.Reason.idprojects;
        $scope.newProjectName = $scope.projectName
        //get thold for this project
        $scope.threshold = parseInt(response.data.Reason.threshold);
        $scope.description = response.data.Reason.description
        $scope.newthreshold = $scope.threshold
        //get the project status
        if (response.data.Reason.status == 'f') {
            $scope.projectStatus = false
        } else {
            $scope.projectStatus = true;
        }
    });
    $scope.desactivate = function () {
        projectService.stop($scope.projectId).then(function (res) {
            $scope.projectStatus = false
        })
    }
    $scope.activate = function () {
        projectService.start($scope.projectId).then(function (res) {
            $scope.projectStatus = true
        })
    }
    $scope.projectnameSave = function () {
        projectService.updateName($scope.projectId, $scope.newProjectName).then(function (res) {
            //refresh the view
            $scope.projectName = $scope.newProjectName;
        })
    }
    $scope.projectdescriptionSave = function () {
        projectService.updateDescription($scope.projectId, $scope.newProjectDescription).then(function (res) {
            $scope.projectDescription = $scope.newProjectDescription;
            $scope.description = $scope.newProjectDescription;
        })
    }
    $scope.projectthresholdSave = function () {
        projectService.updateThold($scope.projectId, $scope.newthreshold).then(function (res) {
            $scope.threshold = $scope.newthreshold
        })
    }



    userService.get($scope.projectId).then(function (res) {

        res.data.Reason.forEach(function (eachUser) {
            accountService.get(eachUser.idaccounts).then(function (response) {
                console.log(response);
                $scope.myusers.push(response.data.Reason[0]);
                console.log($scope.myusers);
            })
        })
        $scope.loadingusers = false;
    })

    //save Equipment
    $scope.createEquipment = function (newEquipmentName) {
        equipmentsService.create($routeParams.idprojects, newEquipmentName).then(function (e, r) {
            console.log(r)
            if (e) {
                //notify the error
                console.log(e)
            } else {
                //push the new equipment to the existing list
                $scope.equipments.push($scope.newEquipment);
            }
            $route.reload();
        })
    }
    //Delete Equipment
    $scope.deleteEq = function (equipmentID) {
        equipmentsService.delete(equipmentID).then(function (e, r) {
            console.log(r)
            $route.reload();
        })
    }
    //Delete user
    $scope.deleteUser = function (userID) {
        console.log("userID " + userID);
        userService.delete(userID, $scope.projectId).then(function (e, r) {
            console.log(r)
            $route.reload();
        })
    }
    //Get Account(not all accounts/only that they are not in the project) to add into project
    userService.getProjectID($routeParams.idprojects).then(function (response) {
        console.log("response" + JSON.stringify(response));
        $scope.specialAccount = response.data.Reason;
        console.log($scope.specialAccount);
    })
    //Save new user
    $scope.addUser = function (user) {
        $scope.idProjects = $routeParams.idprojects;
        $scope.userName = user.eachAccount.username;

        accountService.getAccountbyUserName($scope.userName).then(function (response) {
            $scope.userId = response.data.Reason.idaccounts;

            $scope.newUser = {
                accountid: $scope.userId,
                projectid: $routeParams.idprojects
            }
            console.log($scope.newUser);

            userService.create($scope.newUser).then(function (e, r) {
                console.log(r)
                if (e) {
                    //notify the error
                    console.log(e)
                } else {
                    //push the new user to the existing list
                    $scope.myusers.push($scope.newUser);
                }
                $route.reload();
            })
        })
    };
    $scope.delete = function () {
        projectService.delete($routeParams.idprojects).then(function (res) {
            if (res.data.Result == true) {
                window.location.href = '/steelvalidation/projects';
            }
        })
    }

    var alerttag = function () {

        $scope.$apply(function () {
            $scope.hugeimageSrc = true;
        })

    }
    var alertmarkings = function () {

        $scope.$apply(function () {
            $scope.hugeimageSrcMarkings = true;
        })

    }
    //Add new bar Tag to the project
    $scope.getFile = function (imagename) {
        console.log("Loading Image:" + imagename);
        fileReader.readAsDataURL($scope.file, $scope)//readAsText//readAsDataURL()//readAsBinaryString()//readAsArrayBuffer()
            .then(function (result) {



                switch (imagename) {
                    case 'image':
                        $scope.imageSrc = result;
                        // get the image attributes
                        var image = new Image();

                        image.src = result;
                        image.onload = function () {
                            if (this.width > 900 || this.height > 900) {
                                $scope.hugeimageSrc = true
                            } else {
                                console.log("fixe");
                                $scope.hugeimageSrc = false
                            }
                            $scope.$apply();
                        };
                        break;
                    case 'imageTransport':
                        $scope.imageSrcTransport = result;

                        break;
                    case 'imageMarkings':
                        $scope.imageSrcMarkings = result;
                        // get the image attributes
                        var image = new Image();

                        image.src = result;
                        image.onload = function () {

                            if (this.width > 900 || this.height > 900) {
                                $scope.hugeimageSrcMarkings = true;
                            } else {
                                $scope.hugeimageSrcMarkings = false;
                            }

                            $scope.$apply();
                        };
                        break;
                }
            });
    };
    $scope.insertTag = function (tagType) {

        $scope.idProjects = $routeParams.idprojects;
        //$scope.userName = user.eachAccount.username;

        //accountService.getAccountbyUserName($scope.userName).then(function (response) {
        //$scope.userId = response.data.Reason.idaccounts;
        $scope.newTag = {
            type: $scope.tagType
            , idprojects: $routeParams.idprojects
            , photo: $scope.imageSrc
            , photoTransport: $scope.imageSrcTransport
            , photoMarkings: $scope.imageSrcMarkings
        }

        console.log("newTag:", $scope.newTag);
        //validating form !!!!!!!!!!!!!! 
        if (!$scope.newTag.type || $scope.newTag.type == "") {
            console.log("Missing field type")
            alert("Missing type")
            return
        }
        if (!$scope.newTag.photo || $scope.newTag.photo == "") {
            alert("Missing Tag photo");
            return
        }
        if (!$scope.newTag.photoMarkings || $scope.newTag.photoMarkings == "") {
            alert("Missing Marking photo");
            return
        }

        tagService.create($scope.newTag).then(function (e, r) {
            console.log(r)
            if (e) {
                //notify the error
                console.log(e)

            } else {
                //push the new user to the existing list
                $scope.ProjectTags.push($scope.newTag);

            }
            $route.reload();
        })
    };

    //Delete bar form project
    $scope.deleteTag = function (tagType) {
        console.log("TagType " + tagType);
        tagService.delete(tagType, $scope.projectId).then(function (e, r) {
            console.log(r)
            $route.reload();
        })
    }


    //get bars of the project
    tagService.get($scope.projectId).then(function (response) {
        $scope.ProjectTags = response.data.Reason;
        $scope.loadingbars = false;
    })

    console.log("Loading settingProjectCtrl");
})


angular.module('mainApp').directive("ngFileMarking", function () {
    return {
        link: function ($scope, el) {
            el.bind("change", function (e) {
                $scope.file = (e.srcElement || e.target).files[0];
                $scope.getFile((e.srcElement || e.target).id);
            })
        }
    }
});

angular.module('mainApp').directive("ngFileTag", function () {
    return {
        link: function ($scope, el) {
            el.bind("change", function (e) {
                $scope.file = (e.srcElement || e.target).files[0];
                $scope.getFile((e.srcElement || e.target).id);
            })
        }
    }
});
angular.module('mainApp').factory("fileReader", ["$q", "$log", function ($q, $log) {
    var factory = {};
    factory.readAsDataURL = function (file, scope) {
        var deferred = $q.defer();
        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);
        return deferred.promise;
    };

    var onLoad = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };

    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };

    var getReader = function (deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        return reader;
    };

    return factory;
}]); 