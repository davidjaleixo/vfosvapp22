

uploadUrl = 'http://18.202.25.181:8087/api/identify';
//uploadUrl='http://amehrbod.pythonanywhere.com/api/identify';	
//uploadUrl='http://localhost:5100/api/identify';

var app = angular.module('mainApp');

app.factory('appproxy', function ($q) {
	return {
		proxyIt: function (file) {
			return new Promise(function (resolve, reject) {
				var xhr = new XMLHttpRequest, formdata = new FormData();
				// var deferred = $q.defer();
				formdata.append('file', file);
				xhr.open("POST", '/steelvalidation/api/vf-os-pilot2-vApp2/proxy', true);

				xhr.onload = function () {
					if (this.status >= 200 && this.status <= 300) {
						resolve(xhr.response);
					} else {
						reject({
							status: this.status,
							statustext: xhr.statusText
						})
					}
				}
				xhr.onerror = function () {
					reject({
						status: this.status,
						statusText: xhr.statusText
					})
				}
				// xhr.onreadystatechange = function () {
				// 	console.log("XHR state:", xhr.readyState);
				// 	console.log("XHR status:", xhr.status);
				// 	if (xhr.readyState == 4) {
				// 		if (xhr.status == 200) {
				// 			console.log("Resolving XHR promise...")
				// 			deferred.resolve(xhr)
				// 		}
				// 	} else {
				// 		deferred.reject(xhr)
				// 	}
				// }
				xhr.send(formdata);
			})

			// console.log("ProxyIt:",file);
			// var xhr = new XMLHttpRequest, formdata = new FormData();
			// var deferred = $q.defer();
			// formdata.append('file', file);
			// xhr.open("POST", '/api/vf-os-pilot2-vApp2/proxy', true);
			// xhr.onreadystatechange = function () {
			// 	console.log("XHR state:", xhr.readyState);
			// 	console.log("XHR status:", xhr.status);
			// 	if (xhr.readyState == 4) {
			// 		if (xhr.status == 200) {
			// 			console.log("Resolving XHR promise...")
			// 			deferred.resolve(xhr)
			// 		}
			// 	} else {
			// 		deferred.reject(xhr)
			// 	}
			// }
			// xhr.send(formdata);
			// return deferred.promise;
			// return $http.post('/api/vf-os-pilot2-vApp2/proxy', { file: file })
		}
	}
})
//
// Reusable Uploader service.
//
app.factory('Uploader', function ($q, $rootScope) {
	this.upload = function (url, file) {
		var deferred = $q.defer(),
			formdata = new FormData(),
			xhr = new XMLHttpRequest();
		formdata.append('file', file);
		xhr.onreadystatechange = function (r) {

			if (4 === this.readyState) {
				if (xhr.status == 200) {
					$rootScope.$apply(function () {
						deferred.resolve(xhr);
					});
				} else {
					$rootScope.$apply(function () {
						deferred.reject(xhr);
					});
				}
			}
		}
		xhr.open("POST", url, true);
		xhr.send(formdata);
		return deferred.promise;
	};
	return this;
})

//
// fileChange directive because ng-change doesn't work for file inputs.
//
app.directive('fileChange', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			element.bind('change', function () {
				scope.$apply(function () {
					scope[attrs['fileChange']](element[0].files);
				})
			})
		},
	}
})

//
// controller
//
app.controller('steeltestCtrl', function ($scope, $http, $routeParams, Uploader, tagService, tagHistoryService, projectService, fileReader, appproxy) {

	$scope.loadingimage = false;

	//get bars of the project
	$scope.projectId = $routeParams.idprojects;//1;

	projectService.getProject($routeParams.idprojects).then(function (response) {
		console.log(response.data);
		$scope.projectName = response.data.Reason.name;
	});
	tagService.get($scope.projectId).then(function (response) {
		$scope.ProjectTags = response.data.Reason;
		$scope.ProjectTag = $scope.ProjectTags[0];
	})
	$scope.type = "None";
	$scope.typeshow = "None of Project Defined Bars";
	$scope.manufacturer = "Manufacturer";
	$scope.country = "Country";
	$scope.imagepath = "assets/img/steeltestnotdetected.jpg";
	$scope.iconname = "tag";//"close-circle";
	$scope.colorcode = "danger";


	$scope.imageReady = false;
	$scope.showApprove = false;
	$scope.showReject = false;
	$scope.showNotification = false;
	$scope.showEquipment = false;
	$scope.showSelectEquipment = true;

	$scope.Approve = function (state) {
		$scope.ApproveStatus = "Approved"
		console.log(state)
		if (state) {
			$scope.showApprove = true;
		}
		else {
			$scope.ApproveStatus = "Rejected"
			$scope.showReject = true;
		}

		$scope.CurrentDate = new Date().toString();
		$scope.newTagHistory = {
			bartestprojectid: $scope.projectId,
			bartestapproved: $scope.ApproveStatus,
			bartestdetected: $scope.type,
			bartestok: $scope.ok,
			bartestdatetime: $scope.CurrentDate,
			photoTransport: $scope.imageSrcTransport
		}
		tagHistoryService.create($scope.newTagHistory).then(function (e, r) {
			console.log(r)
			if (e) {
				//notify the error
				console.log(e)
			} else {
				console.log("Test Approval history saved.")
			}
		})
	}

	$scope.uploadFile = function (files) {
		$scope.loadingimage = true;
		console.log("loading image...", files[0]);
		$scope.$apply();

		appproxy.proxyIt(files[0]).then(function (data) {
			console.log("received answer: ", data);

			this.showResult(data);
			$scope.loadingimage = false;
			$scope.$apply();
			alert("Success!");
			

		}).catch(function (err) {
			console.log(err)
			$scope.loadingimage = false;
			$scope.$apply();
			alert("Error:", err)
		})
		// var r = Uploader.upload(uploadUrl, files[0]);
		// r.then(
		// 	function (data) {
		// 		// success
		// 		//console.log(data.response);
		// 		this.showResult(data.response);
		// 		$scope.loadingimage = false;
		// 		alert("Success!")
		// 	},
		// 	function (error) {
		// 		// failure
		// 		console.log(error)
		// 		alert("Error:", error)
		// 	});
	}

	showResult = function (response) {
		//this.presentToast(JSON.parse(response));
		type = JSON.parse(response).SteelType[0];
		$scope.detectedType = type;
		console.log("Server replied:" + type);
		detected = false;
		for (i = 0; i < $scope.ProjectTags.length; i++) {
			if (this.type == $scope.ProjectTags[i].type) {
				console.log("detected");
				$scope.imagepath = $scope.ProjectTags[i].photo;
				$scope.type = $scope.ProjectTags[i].type;
				$scope.typeshow = $scope.ProjectTags[i].type;
				//$scope.manufacturer="Siderurgia";
				//$scope.country="Portugal";
				//$scope.iconname="checkmark-circle-outline";
				//$scope.colorcode="#32db64";	
				$scope.ok = "OK";
				$scope.oktext = "(this type is expected for this project)";
				detected = true;
				$scope.imageReady = true;
				break;
			}
		}
		if (!detected) {
			console.log("not detected");
			$scope.imageReady = true;
			$scope.typeshow = "None of Project defined Bars ";
			$scope.type = "None";
			$scope.ok = "Not OK";
			$scope.oktext = "(this type is not expected for this project)";
			//$scope.manufacturer="Manufacturer";
			//$scope.country="Country";
			$scope.imagepath = "assets/img/steeltestnotdetected.jpg";
			//$scope.iconname="close-circle";
			//$scope.colorcode="danger";
		}
		/*
	  if ((this.type =="A400NRSD") || (this.type=="A400NR"))
		{
			//$scope.manufacturer="Siderurgia";
			console.log("detected type is:"+type);
			$scope.imagepath="/assets/img/A400NRSD.jpg";
			//$scope.country="Portugal";
			//$scope.iconname="checkmark-circle-outline";
			//$scope.colorcode="#32db64";	
			$scope.type="A400";		
		}
		else if ((this.type =="A500NR") || (this.type =="A500NRSD"))
		{
			$scope.manufacturer="Siderurgia";
			$scope.country="Portugal";
			$scope.imagepath="/assets/img/A500NR.jpg";
			$scope.iconname="checkmark-circle-outline";
			$scope.colorcode="#32db64";		
			$scope.type="A500";		
		}
		else //if (this.type =="not detected" or not defined in the project)
		{
			$scope.type ="Nothing";
			$scope.manufacturer="Manufacturer";
			$scope.country="Country";
			$scope.imagepath="/assets/img/steeltestnotdetected.jpg";
			$scope.iconname="close-circle";
			$scope.colorcode="danger";
		}
		*/
	}

	//Add Transport doc
	$scope.getFile = function (imagename) {
		console.log("Loading Image:" + imagename);
		fileReader.readAsDataURL($scope.file, $scope)//readAsText//readAsDataURL()//readAsBinaryString()//readAsArrayBuffer()
			.then(function (result) {
				$scope.imageSrcTransport = result;
			});
	};
});


//options:{
//fileKey: 'file',
//fileName: 'ionicfile.jpg',
//chunkedMode: false,
//mimeType: "multipart/form-data",
//mimeType: "image/jpeg",			
//},
//transformRequest: angular.identity


//directives for Transport document
//Note: Two differetn appraoches are used for loading Transport doc (bellow) and Bar photo (top of this page) 

angular.module('mainApp').directive("ngFile", function () {
	return {
		link: function ($scope, el) {
			el.bind("change", function (e) {
				$scope.file = (e.srcElement || e.target).files[0];
				$scope.getFile((e.srcElement || e.target).id);
			})
		}
	}
});

angular.module('mainApp').factory("fileReader", ["$q", "$log", function ($q, $log) {
	var factory = {};
	factory.readAsDataURL = function (file, scope) {
		var deferred = $q.defer();
		var reader = getReader(deferred, scope);
		reader.readAsDataURL(file);
		return deferred.promise;
	};

	var onLoad = function (reader, deferred, scope) {
		return function () {
			scope.$apply(function () {
				deferred.resolve(reader.result);
			});
		};
	};

	var onError = function (reader, deferred, scope) {
		return function () {
			scope.$apply(function () {
				deferred.reject(reader.result);
			});
		};
	};

	var getReader = function (deferred, scope) {
		var reader = new FileReader();
		reader.onload = onLoad(reader, deferred, scope);
		reader.onerror = onError(reader, deferred, scope);
		return reader;
	};

	return factory;
}]); 