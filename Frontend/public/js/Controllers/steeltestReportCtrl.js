


var app = angular.module('mainApp');

//
// controller
//
app.controller('steeltestReportCtrl', function ($scope, $http, $routeParams, tagHistoryService,projectService) {

	console.log("------------------------------------------------");
	$scope.projectId = $routeParams.idprojects;
	projectService.getProject($routeParams.idprojects).then(function (response) {
		console.log(response.data);
		$scope.projectName = response.data.Reason.name;
	});	

	//get bars test history of the project
	//$scope.projectId = 5;
	tagHistoryService.get($scope.projectId).then(function (response) {
		console.log("------------------------------------------------");
		console.log(response.data.Reason);
		$scope.ProjectTagsHistory = response.data.Reason;
	})


	$scope.setphoto = function (photo) {
		$scope.TPhoto=photo;
	}
});