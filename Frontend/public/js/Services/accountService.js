/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

angular.module('mainApp').factory('accountService', function($http, authService){
	return {
		get: function(accid){
			return $http.get('/api/vf-os-pilot2-vApp4/accounts/getbyID/'+accid)
		},
		getAccountbyUserName: function(userName){
			return $http.get('/api/vf-os-pilot2-vApp4/accounts/getbyUserName/'+userName)
		},
		getAllAccountsOnProject: function(id){
			return $http.get('/api/vf-os-pilot2-vApp4/accounts/getAllAccounts?idaccounts='+id)
		},
		delete: function (accountID) {
			return $http.delete('/api/vf-os-pilot2-vApp4/accounts/' + accountID)
		},
		editAccountName: function (id, name) {
			return $http.patch('/api/vf-os-pilot2-vApp4/accounts/updateName', {id: id, username: name})
		},
		editAccountRole: function (id, role) {
			return $http.patch('/api/vf-os-pilot2-vApp4/accounts/updateRole', {id: id, role: role})
		},
		editAccountPassword: function (id, pass) {
			return $http.patch('/api/vf-os-pilot2-vApp4/accounts/updatePassword', {id: id, pwd: pass})
		}
	}
})