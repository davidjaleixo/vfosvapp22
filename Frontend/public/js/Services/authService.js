/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

angular.module('mainApp')
	.factory('authService', function ($http) {
		var token = null;

		var deleteToken = function () {
			localStorage.removeItem('24token');
			token = null;
		}
		var saveToken = function (token) {
			localStorage.setItem('24token', token);
			token = token;
		}
		var getToken = function () {
			if (!token) {
				return localStorage.getItem('24token');
			}
			return token
		}
		var _getUserDetails = function () {

			let thistoken = getToken();
			let payload


			if (thistoken && thistoken !== 'undefined') {
			// if (thistoken) {
				payload = thistoken.split('.')[1]; //get the token information
				payload = window.atob(payload); //decode token
				return JSON.parse(payload)
			} else {
				return null
			}
		}
		return {
			register: function (usr, pwd) {
				return $http.post('/api/vf-os-pilot2-vApp4/register', { userName: usr, pwd: pwd });
			},
			saveToken: function (token) {
				saveToken(token);
			},
			login: function (credentials) {
				return $http.post('/api/vf-os-pilot2-vApp4/login', credentials)
			},
			isLoggedIn: function () {
				let user = _getUserDetails();
				if (user) {
					return user.exp > Date.now() / 1000; //return true if the session is not expired
				} else {
					return false
				}
			},
			getUserDetails: function () {
				return _getUserDetails();
			},
			logout: function () {
				deleteToken();
			},
			authHeaders: function () {
				let thistoken = getToken();
				if (thistoken) {
					return { headers: { Authorization: `Bearer ${thistoken}` } }
				}
				return null
			}
		}

	})