/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

angular.module('mainApp').factory('equipmentsService', function($http, authService){
	return {
		get: function(pid){
			return $http.get('/api/vf-os-pilot2-vApp4/equipments?idproject='+pid);
		},
		create: function(idProjects, name){
			return $http.post('/api/vf-os-pilot2-vApp4/equipments', {name: name, idProjects:idProjects});
		},
		delete: function(eid){
			return $http.delete('/api/vf-os-pilot2-vApp4/equipments?idequipment='+eid);
		}
	}
})