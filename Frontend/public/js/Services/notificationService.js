/*david.aleixo@knowledgebiz.pt*/

angular.module('mainApp').factory('notificationService', function($http, authService){
	return {
		get: function(){
			return $http.get('/api/vf-os-pilot2-vApp4/notifications/?accid='+authService.getUserDetails().id);
		},
		update: function(body){
			return $http.patch('/api/vf-os-pilot2-vApp4/notifications', body);
		}
	}
})

