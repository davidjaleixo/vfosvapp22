/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

angular.module('mainApp').factory('projectService', function($http, authService){
	return {
		create: function(projectDetails){
			return $http.post('/api/vf-os-pilot2-vApp4/projects/?accid='+authService.getUserDetails().id, projectDetails);
		},
		get: function(){
			return $http.get('/api/vf-os-pilot2-vApp4/projects/?accid='+authService.getUserDetails().id, authService.authHeaders())
		},
		getProject: function (projectID) {
			return $http.get('/api/vf-os-pilot2-vApp4/projects/get?idproject=' + projectID)
		},
		updateName: function(projectid, projectname){
			return $http.patch('/api/vf-os-pilot2-vApp4/projects/updateName', {id: projectid, name:projectname});
		},
		updateDescription: function(projectid, projectdescription){
			return $http.patch('/api/vf-os-pilot2-vApp4/projects/updateDescription', {id: projectid, description:projectdescription});
		},
		stop: function(pid){
			return $http.patch('/api/vf-os-pilot2-vApp4/projects/updateStatus', {id: pid, status: false});
		},
		start: function(pid){
			return $http.patch('/api/vf-os-pilot2-vApp4/projects/updateStatus', {id: pid, status: true});
		},
		updateThold: function(pid, thold){
			return $http.patch('/api/vf-os-pilot2-vApp4/projects/updateThold', {id: pid, thold: thold});
		},
		delete: function(pid){
			return $http.delete('/api/vf-os-pilot2-vApp4/projects?idproject=' + pid);
		}
	}
})