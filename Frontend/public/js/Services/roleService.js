/*made by David Aleixo @ KBZ david.aleixo@knowledgebiz.pt*/

angular.module('mainApp').factory('roleService', function($http, authService){
	return {
		get: function(){
			return $http.get('/api/vf-os-pilot2-vApp4/roles');
		}
	}
})