/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

angular.module('mainApp').factory('slumptestService', function($http, authService){
	return {
		create: function (slumptest) {
			return $http.post('/api/vf-os-pilot2-vApp4/slumptests', slumptest)
		},
		get: function (slumptestID) {
			return $http.get('/api/vf-os-pilot2-vApp4/slumptests?idslumptest=' + slumptestID)
		},
		getSlumptest: function (projectID) {
			return $http.get('/api/vf-os-pilot2-vApp4/slumptests?idproject=' + projectID)
		}
	}
})