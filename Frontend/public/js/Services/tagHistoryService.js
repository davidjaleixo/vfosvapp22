/*made by KBZ */

angular.module('mainApp').factory('tagHistoryService', function($http, authService){
	return {
		create: function (body) {
			/*
			var Headers = {
				headers: {
					'content-type': 'boundary=${form._boundary}'
				}
			}
			*/		
			return $http.post('/api/vf-os-pilot2-vApp2/tagshistory', JSON.stringify(body)/*,Headers*/)
        },      
		get: function (projectid) {
			return $http.get('/api/vf-os-pilot2-vApp2/tagshistory/' + projectid)
        }
    }
})