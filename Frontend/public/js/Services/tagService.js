/*made by KBZ */

angular.module('mainApp').factory('tagService', function($http, authService){
	return {
		create: function (body) {
			/*
			var Headers = {
				headers: {
					'content-type': 'boundary=${form._boundary}'
				}
			}
			*/		
			return $http.post('/api/vf-os-pilot2-vApp2/tags', JSON.stringify(body)/*,Headers*/)
        },
		delete: function (tagType, projectID) {
			return $http.delete('/api/vf-os-pilot2-vApp2/tags/' + tagType +'/'+projectID)
		},      
		get: function (projectid) {
			return $http.get('/api/vf-os-pilot2-vApp2/tags/' + projectid)
        }
    }
})