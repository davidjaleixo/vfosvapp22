/*made by Miguel Rodrigues @ KBZ miguel.rodrigues@knowledgebiz.pt*/

angular.module('mainApp').factory('userService', function($http, authService){
	return {
		create: function (body) {
			return $http.post('/api/vf-os-pilot2-vApp4/users', JSON.stringify(body))
		},
		delete: function (accountID, projectID) {
			return $http.delete('/api/vf-os-pilot2-vApp4/users/' + accountID +'/'+projectID)
		},
		get: function (pid) {
			return $http.get('/api/vf-os-pilot2-vApp4/users/' + pid)
		},
		getProjectID: function (pid) {
			return $http.get('/api/vf-os-pilot2-vApp4/users/getbyNOTPrjctID/' + pid)
		}
	}
})