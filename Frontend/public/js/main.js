
//http interceptor
function vfosInterceptor() {
    return {
        request: function (config) {
            if (config.url.includes("api")) {
                config.url = "/steelvalidation" + config.url;
                console.log("updating api http request to vfos platform...");
                console.log(config);
            }
            return config;
        },

        requestError: function (config) {
            return config;
        },

        response: function (res) {
            return res;
        },

        responseError: function (res) {
            return res;
        }
    }
}



angular.module('mainApp', ['ngRoute', 'ui.bootstrap', 'feRoutes'])
    .factory('vfosInterceptor', vfosInterceptor)
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('vfosInterceptor');
    })

    .run(function ($location, $rootScope, $route, authService) {
        
        $rootScope.$on('$routeChangeStart', function(event, next, current){
            if(next.$$route.protected){
                if(!authService.isLoggedIn()){
                    $location.path('/');
                }
            }
        })
        
        $rootScope.$on("$routeChangeError", function (evt, to, from, error) {
            console.log("routeChangeError with error:",error);

        });
    });
