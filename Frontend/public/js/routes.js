
//Frontend Routes


angular.module('feRoutes', ['ngRoute'])
    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

        $routeProvider
            // home page with the userid as route parameter
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'landingPageCtrl'
            })
            .when('/registration', {
                templateUrl: 'views/registration.html',
                controller: 'registrationCtrl'
            })
            .when('/login', {
                templateUrl: 'views/modal_login.html'
            })
            .when('/projects', {
                templateUrl: 'views/projects.html',
                controller: 'projectCtrl',
                protected: true
            })
            .when('/projects/:idprojects', {
                templateUrl: 'views/projectDetail.html',
                controller: 'projectDetailCtrl',
                protected: true
            })
            .when('/notifications', {
                templateUrl: 'views/notifications.html',
                controller: 'notificationCtrl',
                protected: true
            })
            .when('/project/configuration/:idprojects', {
                templateUrl: 'views/settingProject.html',
                controller: 'settingProjectCtrl',
                protected: true
            })
            .when('/projects/:idprojects/history', {
                templateUrl: 'views/history.html',
                controller: 'historyCtrl',
                protected: true
            })
            .when('/settings', {
                templateUrl: 'views/otherSettings.html',
                controller: 'otherSettingsCtrl',
                protected: true
            })
            .when('/projects/:idprojects/steeltest', {
                templateUrl: 'views/steeltest.html',
                controller: 'steeltestCtrl',
                protected: true
            })
            .when('/projects/:idprojects/report', {
                templateUrl: 'views/steeltestReport.html',
                controller: 'steeltestReportCtrl',
                protected: true
            })
        $locationProvider.html5Mode(true);
    }])