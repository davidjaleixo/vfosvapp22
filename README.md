## VFOS - VAPP24 (VF Concrete Feedback)
### Read me

#### 1. Prerequisite

In order to run this enabler code on your local machine you should have installed Docker.

#### 2. Instalation

Make sure the docker is installed on the system environment.

##### 2.1. Clone the code repository:

`git clone https://username@bitbucket.org/davidjaleixo/vfosvapp24.git`

##### 2.2. Run multi-container Docker application

`This multi-container Docker application is composed by two services: `vApp`.`

`Navigate to the clone directory and execute `docker-compose up --build` to startup the multi-container enabler on background.`
 

#### 2.1. Configuration JSON File

vApp4 will need the following Variables on JSON File:
`{
  "NotificationEnabler":{ 
    "AppID": "notification_appID",
    "DeveloperID": "notification_developerID",
    "AppToken": "notification_token",
    "url_gui":"notification_gui",
    "url_api":"notification_url"
  },
  "RelationalStorage":{
    "URL": "storageComponent_url",
    "ContentType": "storageComponent_contentType",
    "Accept": "storageComponent_accept",
    "Authorization": "storageComponent_authorization",
    "databaseName": "storageComponent_name"
  },
  "Logger":{
    "DEBUGLEVEL": "level_debug"
  },
  "Enabler":{
    "PORT": port
  }
}`

#### 3.0 Usage

Using your favorite web browser please navigate to `localhost:5010` to reach the frontend module.

The following Frontend endpoints are now available:

`http://localhost:5010/`
`http://localhost:5010/registration`
`http://localhost:5010/login`
`http://localhost:5010/projects`
`http://localhost:5010/projects/:idprojects`
`http://localhost:5010/project/configuration/:idprojects`
`http://localhost:5010/projects/:idprojects/history`
`http://localhost:5010/notifications`
`http://localhost:5010/settings`


#### 4. API

## vf-OS vAPP


#### Create Account

```http
POST /api/vf-os-pilot2-vApp4/accounts
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "userName":"my_userName",
    "pwd":"my_password"
}
```

```http
HTTP/1.1 201 OK
"Reason": "Fri, 07 Dec 2018 12:15:58 GMT",
  "server": "Apache-Coyote/1.1",
  "transfer-encoding": "chunked",
  "content-type": "application/json;charset=UTF-8"

{
    "Result": true,
    "Reason": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MCwidXNlcm5hbWUiOiJ0ZXN0QHRlc3RlYWJjLnB0Iiwicm9sZSI6MywiZXhwIjoxNTQ1MDQyMzUwLCJpYXQiOjE1NDQ0Mzc1NTB9.tuaiokHrdBUvDa38AyRtfXWlovjGSQlUQ-GZaKqT1AQ"
}
```

Use this API call whenever is needed to create a new account.

#### Request
POST /api/vf-os-pilot2-vApp4/accounts

#### JSON Body Payload

Name            | Required    | Type   | Description
--------------- | ----------- | ------ | -----------  
userName        |    Yes      | STRING | String is for userName.
pwd             |    Yes      | STRING | String is for password.
 

Example of JSON body payload structure: 
`  {
    "userName":miguel@knowledgebiz.pt,
    "pwd":"QWERTY"
}`

#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason`.

Example:

```json
{
    "Result": true,
    "Reason": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MCwidXNlcm5hbWUiOiJ0ZXN0QHRlc3RlYWJjLnB0Iiwicm9sZSI6MywiZXhwIjoxNTQ1MDQyMzUwLCJpYXQiOjE1NDQ0Mzc1NTB9.tuaiokHrdBUvDa38AyRtfXWlovjGSQlUQ-GZaKqT1AQ"
}
```

#### Return Codes
Code | Description
---- | ----
200  | Token Value.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Delete Account

```http
DELETE /api/vf-os-pilot2-vApp4/accounts/1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:16:44 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Delete Account: Successfully"
}
```

Use this API call whenever is needed to delete account.  

#### Request
`DELETE /api/vf-os-pilot2-vApp4/accounts/:accountID`

#### URL Parameters

Resource Parameter | Description
------------------ | -----------
accountID          | Identifies the Account.

#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's delete specific account:

```json
{
    "Result": true,
    "Reason": "Delete Account: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Delete Account: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.

### vApp24 - Edit Account Name

```http
PATCH /api/vf-os-pilot2-vApp4/accounts/updateName/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_accountID,
    "username":"my_new_username"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:33:58 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Name on Account: Successfully"
}
```

Use this API call whenever is needed to update account's name.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/accounts/updateName/`

#### JSON Body Payload

Name      | Required    | Type    | Description
--------- | ----------- | ------  | -----------  
id        |    Yes      | INTEGER | Identifies the Account.
username  |    Yes      | STRING  | String is for new userName.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific account's name:

```json
{
    "Result": true,
    "Reason": "Update Name on Account: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Name on Account: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Edit Account Role

```http
PATCH /api/vf-os-pilot2-vApp4/accounts/updateRole/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_accountID,
    "role":"my_new_role"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:35:52 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Role on Account: Successfully"
}
```

Use this API call whenever is needed to update account's role.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/accounts/updateRole/`

#### JSON Body Payload

Name  | Required    | Type    | Description
----- | ----------- | ------  | -----------  
id    |    Yes      | INTEGER | Identifies the Account.
role  |    Yes      | INTEGER | Identifies the new Role.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific account's role:

```json
{
    "Result": true,
    "Reason": "Update Role on Account: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Role on Account: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.



### vApp24 - Edit Account's Password

```http
PATCH /api/vf-os-pilot2-vApp4/accounts/updateHashSalt/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_accountID,
    "pwd":"my_new_pwd"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:39:14 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Password on Account: Successfully"
}
```

Use this API call whenever is needed to update account's password.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/accounts/updateHashSalt/`

#### JSON Body Payload

Name  | Required    | Type    | Description
----- | ----------- | ------  | -----------  
id    |    Yes      | INTEGER | Identifies the Accoun.
pwd   |    Yes      | STRING  | String for the new password.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific account's password:

```json
{
    "Result": true,
    "Reason": "Update Password on Account: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Password on Account: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


#### Create Project

```http
POST /api/vf-os-pilot2-vApp4/projects
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "name":"my_projectName",
    "description":"my_description",
    "status":my_status,
    "threshold":my_threshold
}
```

```http
HTTP/1.1 201 OK
"Reason": "Fri, 07 Dec 2018 13:45:58 GMT",
  "server": "Apache-Coyote/1.1",
  "transfer-encoding": "chunked",
  "content-type": "application/json;charset=UTF-8"

{
    "Result": true,
    "Reason": "Create Project: Successfully"

}
```

Use this API call whenever is needed to create a new project.

#### Request
POST /api/vf-os-pilot2-vApp4/projects

#### JSON Body Payload

Name            | Required    | Type   | Description
--------------- | ----------- | ------ | -----------  
name            |    Yes      | STRING | String  is for Name.
description     |    Yes      | STRING | String  is for Description. 
status          |    Yes      | BOOLEAN| Boolean is for status(true or false).
threshold       |    Yes      | INTEGER| Integer is for Threshold Value. 
 

Example of JSON body payload structure: 
`   {
    "name":"project",
    "description":"Description for project",
    "status": true,
    "threshold":15
}`

#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason`.

Example:

```json
{
    "Result": true,
    "Reason": "Create Project: Successfully"

}
```

#### Return Codes
Code | Description
---- | ----
200  | Create Project: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Delete Project

```http
DELETE /api/vf-os-pilot2-vApp4/projects?idproject=1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:23:21 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Delete Project: Successfully"
}
```

Use this API call whenever is needed to delete specific project.  

#### Request
`DELETE /api/vf-os-pilot2-vApp4/projects?idproject=projectID`

#### Query Parameters

Resource Parameter | Description
------------------ | -----------
projectID          | Identifies the project.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's delete project related to specific projectID:

```json
{
    "Result": true,
    "Reason": "Delete Project: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Delete Project: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.



### vApp24 - Edit Project's name

```http
PATCH /api/vf-os-pilot2-vApp4/projects/updateName/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_projectID,
    "name":"my_new_name"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:55:52 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Name of Project: Successfully"
}
```

Use this API call whenever is needed to update project's name.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/projects/updateName/`

#### JSON Body Payload

Name  | Required    | Type    | Description
----- | ----------- | ------  | -----------  
id    |    Yes      | INTEGER | Identifies the Project.
name  |    Yes      | STRING  | String for the new name.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific projects's name:

```json
{
    "Result": true,
    "Reason": "Update Name of Project: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Name of Project: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Edit Project's Description

```http
PATCH /api/vf-os-pilot2-vApp4/projects/updateDescription/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_projectID,
    "description":"my_new_description"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 18:01:00 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Description of Project: Successfully"
}
```

Use this API call whenever is needed to update project's Description.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/projects/updateDescription/`

#### JSON Body Payload

Name         | Required    | Type    | Description
------------ | ----------- | ------  | -----------  
id           |    Yes      | INTEGER | Identifies the Project.
description  |    Yes      | STRING  | String for the new description.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific projects's description:

```json
{
    "Result": true,
    "Reason": "Update Description of Project: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Description of Project: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Edit Project's status

```http
PATCH /api/vf-os-pilot2-vApp4/projects/updateStatus/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_projectID,
    "status":"my_new_status"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 18:04:05 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Status of Project: Successfully"
}
```

Use this API call whenever is needed to update project's Status.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/projects/updateStatus/`

#### JSON Body Payload

Name    | Required    | Type    | Description
------- | ----------- | ------  | -----------  
id      |    Yes      | INTEGER | Identifies the Project.
status  |    Yes      | BOOLEAN | Boolean for the new status(false or true).


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific projects's status:

```json
{
    "Result": true,
    "Reason": "Update Status of Project: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Status of Project: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Edit Project's Threshold Value

```http
PATCH /api/vf-os-pilot2-vApp4/projects/updateThold/
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "id":my_projectID,
    "thold":"my_new_thold"
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 18:06:12 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Threshold of Project: Successfully"
}
```

Use this API call whenever is needed to update project's Status.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/projects/updateThold/`

#### JSON Body Payload

Name    | Required    | Type    | Description
------- | ----------- | ------  | -----------  
id      |    Yes      | INTEGER | Identifies the Project.
thold   |    Yes      | INTEGER | Integer for the new Threshold Value.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit specific projects's threshold:

```json
{
    "Result": true,
    "Reason": "Update Threshold of Project: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Threshold of Project: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Create User

```http
POST /api/vf-os-pilot2-vApp4/users
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "accountid":my_accountID,
    "projectid":my_projectID
}
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 14:14:43 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Create users: Successfully"
}
```

Use this API call whenever is needed to create user.  

#### Request
`POST /api/vf-os-pilot2-vApp4/users`


#### JSON Body Payload

Name        | Required    | Type    | Description
----------- | ----------- | ------- | ----------------------------------  
accountid   |    Yes      | INTEGER | Identifies the Account.
projectid   |    Yes      | INTEGER | Identifies the Project.  

Example of JSON body payload structure: 
`{
    "accountid":1,
    "projectid":1
}`


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's to create one user:

```json
{
    "Result": true,
    "reason": "Create users: Successfully"
}
```

#### Return Codes
Code | Description
---- | -----------
200  | Create users: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Delete User

```http
DELETE /api/vf-os-pilot2-vApp4/users/1/1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 14:14:43 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Delete User: Successfully"
}
```

Use this API call whenever is needed to delete user.  

#### Request
`DELETE /api/vf-os-pilot2-vApp4/users/:accountID/:projectID`

#### URL Parameters

Resource Parameter | Description
------------------ | -----------
accountID          | Identifies the Account.
projectID          | Identifies the Project.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's delete specific user:

```json
{
    "Result": true,
    "Reason": "Delete User: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Delete User: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Get All Users

```http
GET /api/vf-os-pilot2-vApp4/users/1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Date: Fri, 07 Dec 2018 12:57:31 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
    "Result": true,
    "Reason": [
        {
            "idusers": my_userID,
            "idaccounts": my_accountID,
            "idprojects": my_projectID
        }
    ]
}
```

Use this API call whenever is needed to retrieve users related on specific project.  

#### Request
`GET /api/vf-os-pilot2-vApp4/users/:projectid`


#### URL Parameters

Resource Parameter | Description
------------------ | -----------
projectid          | Identifies the Project.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the subjects of users.

In the following Example it's queried all users related to specific projectID:

```json
{
    "Result": true,
    "Reason": [
        {
            "idusers": "1",
            "idaccounts": "1",
            "idprojects": "1"
        }
    ]
}
```

#### Return Codes
Code | Description
---- | -----------
200  | Data Found.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Get Role

```http
GET /api/vf-os-pilot2-vApp4/roles
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Date: Tue, 29 Aug 2018 15:00:00 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
    "Result": true,
    "Reason": [
        {
            "accounttype": "my_accounttype",
            "description": "my_description",
            "idroles": my_roleID
        }
    ]
}
```

Use this API call whenever is needed to retrieve roles.  

#### Request
`GET /api/vf-os-pilot2-vApp4/roles`


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the subjects of roles.


In the following Example it's queried all roles:

```json
{
    "Result": true,
    "Reason": [
        {
            "accounttype": "contractor",
            "description": "Contractor",
            "idroles": "1"
        },
        {
            "accounttype": "admin",
            "description": "Administrator",
            "idroles": "2"
        },
        {
            "accounttype": "provider",
            "description": "Provider",
            "idroles": "3"
        }
    ]
}
```


#### Return Codes
Code | Description
---- | -----------
200  | Data Found.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


#### Create Equipment

```http
POST /api/vf-os-pilot2-vApp4/equipments
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "name":"my_name",
    "idProjects":my_projectID
}
```

```http
HTTP/1.1 201 OK
"Reason": "Fri, 07 Dec 2018 14:26:58 GMT",
  "server": "Apache-Coyote/1.1",
  "transfer-encoding": "chunked",
  "content-type": "application/json;charset=UTF-8"

{
    "Result": true,
    "Reason": "Create Equipment: Successfully"

}
```

Use this API call whenever is needed to create a new equipments.

#### Request
POST /api/vf-os-pilot2-vApp4/equipments

#### JSON Body Payload

Name            | Required    | Type   | Description
--------------- | ----------- | ------ | -----------  
name            |    Yes      | STRING | String is for Name.
idProjects      |    Yes      | INTEGER| Identifies the Project. 
 

Example of JSON body payload structure: 
`   {
    "name":"c1",
    "idProjects":1
}`

#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason`.

Example:

```json
{
    "Result": true,
    "Reason": "Create Equipment: Successfully"

}
```

#### Return Codes
Code | Description
---- | ----
200  | Create Equipment: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Delete Equipment

```http
DELETE /api/vf-os-pilot2-vApp4/equipments?idequipment=1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 17:34:00 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Delete Equipment: Successfully"
}
```

Use this API call whenever is needed to delete specific Equipment.  

#### Request
`DELETE /api/vf-os-pilot2-vApp4/equipments?idequipment=equipmentID`

#### Query Parameters

Resource Parameter | Description
------------------ | -----------
equipmentID        | Identifies the Equipment.

#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's delete specific Equipment:

```json
{
    "Result": true,
    "Reason": "Delete Equipment: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Delete Equipment: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


#### Create Slumptests

```http
POST /api/vf-os-pilot2-vApp4/slumptests
Accept: */*
Content-Type: application/json; charset=utf-8

{
    "value":my_value,
    "equipment":my_equipment,
    "project":my_project,
    "account":my_account
}
```

```http
HTTP/1.1 201 OK
"Reason": "Fri, 07 Dec 2018 14:26:58 GMT",
  "server": "Apache-Coyote/1.1",
  "transfer-encoding": "chunked",
  "content-type": "application/json;charset=UTF-8"

{
    "Result": true,
    "Reason": "Create SlumpTest: Successfully"

}
```

Use this API call whenever is needed to create a new slumptests.

#### Request
POST /api/vf-os-pilot2-vApp4/slumptests

#### JSON Body Payload

Name         | Required    | Type   | Description
------------ | ----------- | ------ | -----------  
value        |    Yes      | INTEGER| Integer for the Slumptest Value.
equipment    |    Yes      | INTEGER| Identifies the Equipment. 
project      |    Yes      | INTEGER| Identifies the Project.
account      |    Yes      | INTEGER| Identifies the Account.  
 

Example of JSON body payload structure: 
`  {
    "value": 12,
    "idEquipments":2,
    "idProjects":1,
    "idaccounts":1
}`

#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason`.

Example:

```json
{
    "Result": true,
    "Reason": "Create SlumpTest: Successfully"

}
```

#### Return Codes
Code | Description
---- | ----
200  | Create SlumpTest: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Get Slumptest

```http
GET /api/vf-os-pilot2-vApp4/slumptests?idslumptest=1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Date: Fri, 07 Dec 2018 16:58:00 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
    "Result": true,
    "data": {
        "value": "4",
        "date": "2018-12-06 17:20:40.144515+00",
        "idslumptests": "1",
        "idprojects": "1",
        "idequipments": "1",
        "idaccounts": "1"
    }
}
```

Use this API call whenever is needed to retrive slumptests.  

#### Request
`GET /api/vf-os-pilot2-vApp4/slumptests?idslumptest=slumptestID`


#### Query Parameters

Resource Parameter | Description
------------------ | -----------
slumptestID        | Identifies the slumptest.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the subjects of slumptests.

In the following Example it's queried related to specific slumptests:

```json

{
    "Result": true,
    "data": {
        "value": "4",
        "date": "2018-12-06 17:20:40.144515+00",
        "idslumptests": "1",
        "idprojects": "1",
        "idequipments": "1",
        "idaccounts": "1"
    }
}
```

#### Return Codes
Code | Description
---- | -----------
200  | Data Found.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Get Notifications

```http
GET /api/vf-os-pilot2-vApp4/notifications?accid=1
Accept: */*
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Date: Fri, 07 Dec 2018 14:55:00 GMT
Connection: keep-alive
Transfer-Encondig: chunked

[
    {
        "read": "t",
        "date": "2018-12-06 17:20:40.144515+00",
        "idnotification": "1",
        "idaccounts": "1",
        "idslumptests": "2"
    }
]
```

Use this API call whenever is needed to retrive notifications.  

#### Request
`GET /api/vf-os-pilot2-vApp4/notifications?accid=accountID`

#### Query Parameters

Resource Parameter | Description
------------------ | -----------
accountID          | Identifies the account.


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the subjects of notifications.

In the following Example it's queried all statistics related to specific notifications:

```json

[
    {
        "read": "t",
        "date": "2018-12-06 17:20:40.144515+00",
        "idnotification": "1",
        "idaccounts": "1",
        "idslumptests": "2"
    }
]
```

#### Return Codes
Code | Description
---- | -----------
200  | Data Found.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### vApp24 - Edit Notification

```http
PATCH /api/vf-os-pilot2-vApp4/notifications
Accept: */*
Content-Type: application/json; charset=utf-8
[
    {
        "idnotification":my_notificationID,
        "read":"my_read"
    },
    {
        "idnotification":my_notificationID,
        "read":"my_read"
    }
]
```


```http
HTTP/1.1 200 OK
Content-type: application/json
X-Powered-By: Express
Vary: X-HTTP-Method-Override
Date: Fri, 07 Dec 2018 18:03:58 GMT
Connection: keep-alive
Transfer-Encondig: chunked

{
  "Result": true,
  "Reason": "Update Notifications State: Successfully"
}
```

Use this API call whenever is needed to update notification.  

#### Request
`PATCH /api/vf-os-pilot2-vApp4/notifications`

#### JSON Body Payload

Name             | Required    | Type    | Description
---------------- | ----------- | ------  | -----------  
idnotification   |    Yes      | INTEGER | Identifies the Notification.
read             |    Yes      | BOOLEAN | Boolean for the read(false or true).


#### Return Payload

The API response will contain a JSON document with the property `Result`: **true**, **false** and the `Reason` with the successfully or not successfully message.


In the following Example it's edit notifocation 

```json
{
    "Result": true,
    "Reason": "Update Notifications State: Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Notifications State: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### Powered by:

![alt text](https://static.wixstatic.com/media/d65bd8_d460ab5a6ff54207a8ac3e7497af18c4~mv2_d_4201_2594_s_4_2.png "VF Concrete Feedback (vApp24))")Successfully"
}

```

#### Return Codes
Code | Description
---- | -----------
200  | Update Notifications State: Successfully.
500  | Internal Server Error - There was an unexpected error at some point during the processing of the request.


### Powered by:

![alt text](https://static.wixstatic.com/media/d65bd8_d460ab5a6ff54207a8ac3e7497af18c4~mv2_d_4201_2594_s_4_2.png "VF Concrete Feedback (vApp24))")